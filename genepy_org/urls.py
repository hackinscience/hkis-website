"""hkis URL Configuration."""

import debug_toolbar
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path

urlpatterns = [
    # Uncomment to work on the 404 template:
    # path(
    #     "404/",
    #     TemplateView.as_view(template_name="404.html"),
    #     name="preview_404",
    # ),
    path("accounts/", include("registration.backends.simple.urls")),
    path("admin/pages/", include("hkis.admin")),
    path("admin/", admin.site.urls),
    path("__debug__/", include(debug_toolbar.urls)),
    path("", include("hkis.urls")),
]


urlpatterns += staticfiles_urlpatterns()
