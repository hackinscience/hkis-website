"""Correction bot.

Do not run the bot with a venv located in /home or /tmp, it won't
work, those directories are hidden during sandboxed execution.

Run it as `./manage.py bot` if you want guidance.
"""

import json
import logging
import os
import shlex
import sys
import tempfile
import time
from collections import deque
from pathlib import Path
from random import choice
from subprocess import DEVNULL, PIPE, STDOUT, Popen, TimeoutExpired
from typing import Optional

import redis

logger = logging.getLogger(__name__)

FIREJAIL_OPTIONS = [
    "--deterministic-exit-code",
    "--quiet",
    "--allow-debuggers",  # To run valgrind ♥
    "--net=none",
    "--x11=none",
    "--protocol=inet",
    "--private-dev",
    "--private-tmp",
    "--caps.drop=all",
    "--noprofile",
    "--nonewprivs",
    "--nosound",
    "--no3d",
    "--nogroups",
    "--noroot",
    "--seccomp",
    "--rlimit-fsize=16777216",  # 32768 is enough for Python exercises.
    "--rlimit-nofile=100",  # but cc needs way more (rust exercises).
    "--rlimit-nproc=2000",
    "--rlimit-cpu=20",
    "--rlimit-as=1610612736",  # 1.5GB. correction_helper will cap
    # student code at 1GB, leaving some space for check.py to report
    # errors.
    "--blacklist=/var",
    "--blacklist=/sys",
    "--blacklist=/boot",
]


def congrats(language):
    """Generates a congratulation sentence."""
    return (
        choice(
            {
                "en": [
                    "Congrats",
                    "Nice job",
                    "Well done",
                    "Spot on",
                    "Bravo",
                    "Nice",
                    "Good",
                ],
                "fr": [
                    "Bravo",
                    "Bien joué",
                    "Super",
                    "Excellent",
                    "Joli",
                ],
            }[language]
        )
        + choice(
            {
                "en": ["! ", "!! ", "!!! ", "! ! "],
                "fr": [" ! ", " !! ", " !!! ", " ! ! "],
            }[language]
        )
        + choice(
            {
                "en": [
                    "Your exercise is OK.",
                    "Right answer.",
                    "Good answer.",
                    "Correct answer.",
                    "Looks good to me!",
                    "Your answer is right.",
                    "Your answer is correct.",
                ],
                "fr": [
                    "C'est juste.",
                    "Bonne réponse.",
                    "Correct.",
                    "Ça me semble bon.",
                    "C'est la bonne réponse.",
                    "Excellente réponse.",
                ],
            }[language]
        )
    )


def run_in_jail(cmd, files: dict, env: Optional[dict] = None):
    """Run cmd in a temporary directory, populated with the given files,
    and the given env."""
    with tempfile.TemporaryDirectory(prefix="hkis") as tmpdir:
        for file_name, file_content in files.items():
            (Path(tmpdir) / file_name).write_text(file_content, encoding="UTF-8")
        firejail_env = os.environ.copy()
        if env:
            firejail_env.update(env)
        argv = ["firejail"] + FIREJAIL_OPTIONS + ["--private=" + tmpdir] + cmd
        logger.debug("Running: %s", shlex.join(argv))
        jailed_proc = Popen(  # pylint: disable=consider-using-with
            argv,
            stdin=DEVNULL,
            stdout=PIPE,
            stderr=STDOUT,
            cwd=tmpdir,
            env=firejail_env,
        )
        try:
            stdout = (
                jailed_proc.communicate(timeout=40)[0]
                .decode("UTF-8", "backslashreplace")
                .replace("\u0000", r"\x00")
                .replace(  # Simplify tracebacks by hiding the temporary directory
                    'File "' + os.path.expanduser("~/"), 'File "'
                )
            )[:65_536]
            if jailed_proc.returncode == 0:
                return True, stdout
            if jailed_proc.returncode == 255:
                return False, "Checker timed out, look for infinite loops maybe?"
            return (
                False,
                stdout
                or f"Correction script exited unexpectedly with code {jailed_proc.returncode}.",
            )
        except TimeoutExpired:
            jailed_proc.kill()
            jailed_proc.wait()
            return False, "Checker timed out."
        except MemoryError:
            return False, "Not enough memory to run your code."


INTERPRETER = os.environ.get("CORRECTION_BOT_INTERPRETER", sys.executable)


def check_answer(answer: dict):
    """Answer should contain: check, source_code, and language."""
    logger.info(
        "%s %s Checking an answer...",
        answer.get("user", ""),
        answer.get("exercise_slug", ""),
    )
    answer["is_valid"], answer["correction_message"] = run_in_jail(
        [INTERPRETER, "-u", "./check.py"],
        files={
            "check.py": answer["check_py"],
            "solution": answer["source_code"],
        },
        env={"LANGUAGE": answer["language"]} if "language" in answer else {},
    )
    logger.info(
        "%s %s answer corrected.",
        answer.get("user", ""),
        answer.get("exercise_slug", ""),
    )

    if answer["is_valid"] and not answer["correction_message"]:
        answer["correction_message"] = congrats(answer.get("language", "en"))


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--redis-url", help="Redis URL like: redis://genepy.org:6379/0")
    return parser.parse_args()


class RedisCorrectionBot:
    """Correction bot getting work over Redis."""

    def __init__(self, url):
        self.name = os.uname().nodename
        self.start_time = time.time()
        self.number_of_exercises_corrected = 0
        self.last_time_i_worked = None
        self.delays = deque((), maxlen=10)  # Store duration of last 10 jobs
        if url:
            self.connection = redis.Redis.from_url(url)
        else:
            self.connection = redis.Redis()

    def run_forever(self):
        logger.info("Started correction_bot.")
        while True:
            logger.info("Waiting for answers…")
            self.report_status()
            self.run_once()

    def store_timings(self, before_waiting, after_waiting, after_working):
        self.delays.append(
            {
                "at": time.time(),
                "time_spent_waiting": after_waiting - before_waiting,
                "time_spent_working": after_working - after_waiting,
            }
        )

    def run_once(self):
        before_waiting = time.perf_counter()
        popped = self.connection.blpop("answers", timeout=120)
        after_waiting = time.perf_counter()
        if popped is None:
            self.store_timings(before_waiting, after_waiting, time.perf_counter())
            return
        _key, message = popped
        answer = json.loads(message.decode("ASCII"))
        check_answer(answer)
        self.connection.publish(
            answer["reply_to"],
            json.dumps(answer, ensure_ascii=True).encode("ASCII"),
        )
        self.number_of_exercises_corrected += 1
        self.last_time_i_worked = time.time()
        self.store_timings(before_waiting, after_waiting, time.perf_counter())

    @property
    def status(self) -> dict:
        return {
            "uptime_seconds": time.time() - self.start_time,
            "last_status_report": time.time(),
            "number_of_exercises_corrected": self.number_of_exercises_corrected,
            "last_time_i_worked": self.last_time_i_worked,
            "delays": list(self.delays),
        }

    def report_status(self):
        self.connection.hset(
            "correction_bot_status",
            self.name,
            json.dumps(self.status, ensure_ascii=True).encode("ASCII"),
        )


def verify_config():
    logging.debug("Using Python interpreter: %r", INTERPRETER)
    success, stdout = run_in_jail([INTERPRETER, "-c", "print('OK')"], {})
    if not success:
        print(stdout)
        print(
            """Ensure that the Python executable is **not** located in /home or /tmp

The sandbox hides `/home` and `/tmp` during execution, so if Python
(or its venv) is in here, it can't be executed."""
        )
        exit(1)


def main():
    """Module entry point, pull answer from redis, and publish back the ansewrs."""
    args = parse_args()
    logging.basicConfig(level=logging.DEBUG)
    verify_config()
    bot = RedisCorrectionBot(args.redis_url)
    bot.run_forever()


if __name__ == "__main__":
    main()
