import os
import sys
from pathlib import Path
from subprocess import run

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Run the correction bot"

    def handle(self, **options):
        if not Path("/srv/bot/").exists():
            print(
                """I need a directory outside /home and /tmp, please run:

    sudo mkdir /srv/bot
    sudo chown $USER:$USER /srv/bot/
"""
            )
            sys.exit(1)
        # Never use Python from ~/.local/, they are hidden by firejail
        real_interpreter = "/usr/bin/python"
        run([real_interpreter, "-m", "venv", "/srv/bot/venv/"], check=True)
        run(
            [
                "/srv/bot/venv/bin/python",
                "-m",
                "pip",
                "install",
                "redis",
                "correction_helper",
            ],
            check=True,
        )
        os.execv(
            "/srv/bot/venv/bin/python",
            ["/srv/bot/venv/bin/python", settings.BASE_DIR / "correction_bot.py"],
        )
