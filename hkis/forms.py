from django import forms
from django_ace import AceWidget

from hkis.models import Answer


class AnswerForm(forms.ModelForm):
    def __init__(self, language="python", **kwargs):
        """Language allow to choose for which language the widget colorize code."""
        super().__init__(**kwargs)
        self.fields["source_code"].widget.mode = language

    class Meta:
        model = Answer
        fields = ["source_code", "exercise"]
        widgets = {
            "source_code": AceWidget(
                theme="twilight",
                width=None,
                height=None,
                fontsize="16px",
                toolbar=False,
                showgutter=True,
                behaviours=False,
                basicautocompletion=True,
            ),
            "exercise": forms.HiddenInput(),
        }
