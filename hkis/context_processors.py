from django.conf import settings

from hkis.models import Membership, Team


def genepy(request):
    context = {
        "version": settings.GIT_HEAD,
    }
    if not request.user.is_anonymous:
        context["todo"] = Membership.objects.filter(
            role=Membership.Role.PENDING,
            team__in=Team.objects.my_teams(request.user),
        ).count()

    return context
