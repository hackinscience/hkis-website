import datetime as dt

import django.contrib.auth.admin
from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import Group
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.db.models import Exists, OuterRef
from django.urls import path, reverse
from django.utils.functional import cached_property
from django.utils.html import escape, format_html
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, TemplateView
from django_ace import AceWidget
from modeltranslation.admin import TranslationAdmin

from hkis.models import (
    Answer,
    CorrectionBotStats,
    Exercise,
    Membership,
    Page,
    Tag,
    Team,
    User,
)


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        fields = ("title", "body", "position", "in_menu", "language")
        widgets = {
            "body": AceWidget(
                mode="markdown",
                theme="twilight",
                width=None,
                height=None,
                toolbar=False,
            ),
        }


class AdminExerciseForm(forms.ModelForm):
    class Meta:
        model = Exercise
        fields = (
            "title",
            "author",
            "check_py",
            "is_published",
            "wording",
            "initial_solution",
            "position",
            "points",
            "page",
        )
        widgets = {
            "check_py": AceWidget(
                mode="python", theme="twilight", width=None, height=None, toolbar=False
            ),
            "wording": AceWidget(
                mode="markdown",
                theme="twilight",
                width=None,
                height=None,
                toolbar=False,
            ),
            "initial_solution": AceWidget(
                mode="python", theme="twilight", width=None, height=None, toolbar=False
            ),
        }


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = (
            "exercise",
            "source_code",
            "is_corrected",
            "is_valid",
            "is_shared",
            "correction_message",
            "corrected_at",
            "is_unhelpfull",
            "votes",
        )
        widgets = {
            "source_code": AceWidget(
                mode="python", theme="twilight", width=None, height=None, toolbar=False
            ),
            "correction_message": AceWidget(
                mode="markdown",
                theme="twilight",
                width=None,
                height=None,
                toolbar=False,
            ),
        }


class ExercisePageFilter(admin.SimpleListFilter):
    title = _("page")
    parameter_name = "page"

    def lookups(self, request, model_admin):
        return [
            (page.pk, page.title)
            for page in Page.objects.editable_by(request.user).filter(
                Exists(Exercise.objects.filter(page=OuterRef("pk")))
            )
        ]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(page=self.value())
        return None


class TagFilter(admin.SimpleListFilter):
    title = _("tag")
    parameter_name = "tag"

    def lookups(self, request, model_admin):
        return [
            (tag.id, tag.title)
            for tag in Tag.objects.filter(
                Exists(Exercise.objects.filter(tags=OuterRef("pk")))
            )
        ]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(tags=self.value())
        return None


class TagsInline(admin.TabularInline):
    model = Exercise.tags.through
    extra = 1


class ExerciseAdmin(TranslationAdmin):
    list_filter = (ExercisePageFilter, TagFilter)
    inlines = (TagsInline,)
    autocomplete_fields = ("author", "page")
    search_fields = ("title",)
    fields = (
        "title",
        "slug",
        "author",
        "page",
        "position",
        "created_at",
        "is_published",
        "points",
        "wording",
        "initial_solution",
        "check_py",
    )
    form = AdminExerciseForm
    list_display = [
        "__str__",
        "title",
        "formatted_position",
        "points",
        "is_published",
        "_tags",
    ]
    ordering = ("position",)
    readonly_fields = ("id", "created_at")

    def _tags(self, obj):
        return ", ".join(tag.title for tag in obj.tags.all())

    def get_queryset(self, request):
        """If not superuser, one can only see own exercises."""
        queryset = (
            super()
            .get_queryset(request)
            .select_related("page")
            .prefetch_related("tags")
        )
        if request.user.is_superuser:
            return queryset
        return queryset.filter(author=request.user)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            if not change:
                obj.author = request.user
        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return self.readonly_fields
        return self.readonly_fields + ("author",)

    @admin.display(description="position")
    def formatted_position(self, obj):
        return f"{obj.position:.2f}"


class PageAdmin(TranslationAdmin):
    autocomplete_fields = ("author",)
    search_fields = ("title_en", "title_fr")
    form = PageForm
    fields = [
        "title_en",
        "title_fr",
        "author",
        "language",
        "position",
        "in_menu",
        "body_en",
        "body_fr",
    ]
    list_display = ["url", "title", "author"]

    def get_queryset(self, request):
        return Page.objects.editable_by(request.user).select_related("author")

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            if not change:
                obj.author = request.user
        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return self.readonly_fields
        return self.readonly_fields + ("author", "in_menu")

    def url(self, obj):
        return "/" + obj.slug + "/"


class MembershipInline(admin.TabularInline):
    verbose_name = _("Member")
    model = Membership
    autocomplete_fields = ("user",)
    extra = 1


class TeamAdmin(admin.ModelAdmin):
    fields = ("name", "is_public", "slug")
    list_display = ("name", "points", "members_qty")
    ordering = ("-points",)
    readonly_fields = ("created_at", "slug")
    inlines = (MembershipInline,)

    def members_qty(self, team):
        return team.members.count()

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not change:
            obj.add_member(request.user)

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Team.objects.all()
        else:
            return Team.objects.my_teams(request.user)


@admin.action(description="Send to correction bot")
def send_to_correction_bot(
    modeladmin, request, queryset
):  # pylint: disable=unused-argument
    for answer in queryset:
        answer.send_to_correction_bot()


class AnswerTeamFilter(admin.SimpleListFilter):
    title = _("team")
    parameter_name = "team"

    def lookups(self, request, model_admin):
        return [(team.id, team.name) for team in Team.objects.my_teams(request.user)]

    def queryset(self, request, queryset):
        if self.value() is None:
            return None
        return queryset.filter(user__teams=self.value()).exclude(user=request.user)


class UserTeamFilter(admin.SimpleListFilter):
    title = _("team")
    parameter_name = "team"

    def lookups(self, request, model_admin):
        return [(team.id, team.name) for team in Team.objects.my_teams(request.user)]

    def queryset(self, request, queryset):
        if self.value() is None:
            return None
        return queryset.filter(teams=self.value())


class AnswerPageFilter(admin.SimpleListFilter):
    """Filtering exercises by page.

    It's like filtering on exericise__page but not showing pages
    having no exercises.
    """

    title = _("page")
    parameter_name = "page"

    def lookups(self, request, model_admin):
        return [
            (page.pk, page.title)
            for page in Page.objects.editable_by(request.user).filter(
                Exists(Exercise.objects.filter(page=OuterRef("pk")))
            )
        ]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(exercise__page=self.value())
        return queryset


class AnswerExerciseFilter(admin.SimpleListFilter):
    """Filtering answers by exercise.

    It presents exercise titles but filter by exercise pk.
    """

    title = _("exercise")
    parameter_name = "exercise"

    def lookups(self, request, model_admin):
        return [
            (exercise.pk, str(exercise))
            for exercise in Exercise.objects.filter(is_published=True).select_related(
                "page"
            )
        ]

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(exercise__pk=self.value())
        return queryset


class DumbPaginator(Paginator):
    """Paginator not spending time actually counting how many rows exists.

    Problem with the real Paginator is it runs a count(*) on every page
    "just" to know how many page exists, this is nice and pretty on the UI.

    But with searches like exercices with '/answer/?q=hello', as we
    search the string in user names, exercise title and user teams
    names, it can get very, slow.
    """

    @cached_property
    def count(self):
        return 9999


class AnswerAdmin(admin.ModelAdmin):
    readonly_fields = (
        "user",
        "created_at",
        "corrected_at",
        "is_corrected",
        "is_valid",
        "is_shared",
        "votes",
        "exercise",
    )
    list_display_links = ("user", "exercise")
    show_full_result_count = False
    paginator = DumbPaginator
    actions = (send_to_correction_bot,)
    list_display = (
        "user",
        "exercise",
        "is_valid",
        "short_correction_message",
        "created_at",
        "is_corrected",
    )
    list_filter = (
        AnswerTeamFilter,
        AnswerPageFilter,
        "is_unhelpfull",
        "is_corrected",
        "is_valid",
        "is_shared",
        AnswerExerciseFilter,
    )
    search_fields = ("user__username",)
    search_help_text = _("Search answers by student username.")
    form = AnswerForm

    def get_queryset(self, request):
        return Answer.objects.visible_by(request.user)


class IsTeacherFilter(admin.SimpleListFilter):
    title = _("group")
    parameter_name = "group"

    def lookups(self, request, model_admin):
        return [(group.pk, group.name) for group in Group.objects.all()]

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(groups=Group.objects.get(pk=self.value()))


class TagAdmin(TranslationAdmin):
    list_display = ["title", "position"]


class EmailTestForm(forms.Form):
    email = forms.EmailField()

    def send_email(self):
        send_mail(
            "Test message",
            "This is a test message.",
            settings.SERVER_EMAIL,
            [self.cleaned_data["email"]],
            fail_silently=False,
        )


class ErrorTestForm(forms.Form):
    message = forms.CharField(max_length=512)


class SuperuserAdminView(UserPassesTestMixin, TemplateView):
    title = "(no title)"

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            **admin.site.each_context(self.request),
            "title": self.title,
        }


class EmailTestView(SuperuserAdminView, FormView):
    template_name = "hkis/admin/emailtest.html"
    form_class = EmailTestForm
    success_url = "/admin/pages/email-test"
    title = "Email test"

    def form_valid(self, form):
        form.send_email()
        return super().form_valid(form)


class ErrorTestView(SuperuserAdminView, FormView):
    template_name = "hkis/admin/errortest.html"
    form_class = ErrorTestForm
    title = "Error test"

    def form_valid(self, form):
        raise ValueError(form.data["message"])


class DebugToolbarView(SuperuserAdminView):
    template_name = "hkis/admin/debug_toolbar.html"
    title = "Debug toolbar"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data["debug_toolbar_is_enabled"] = (
            settings.DEBUG_TOOLBAR_SECRET in self.request.headers.get("cookie", "")
        )
        data["debug_toolbar_secret"] = settings.DEBUG_TOOLBAR_SECRET
        return data


class CorrectionBotsView(SuperuserAdminView):
    template_name = "hkis/admin/bots.html"
    title = "Correction servers"

    def get_context_data(self, **kwargs):
        import redis  # pylint: disable=import-outside-toplevel

        data = super().get_context_data(**kwargs)
        raw_stats = redis.Redis().hgetall("correction_bot_status")
        stats = [CorrectionBotStats(key, value) for key, value in raw_stats.items()]
        two_days_ago = dt.datetime.now(dt.UTC) - dt.timedelta(days=2)
        recent_stats = [
            stat for stat in stats if stat.last_time_i_worked > two_days_ago
        ]
        recent_stats.sort(key=lambda stat: stat.name)
        return data | {"stats": recent_stats}


class UserAdmin(django.contrib.auth.admin.UserAdmin):
    list_display = (
        "username",
        "email",
        "is_staff",
        "formatted_points",
        "public_profile",
        "show_in_leaderboard",
    )
    list_filter = django.contrib.auth.admin.UserAdmin.list_filter + (
        UserTeamFilter,
        IsTeacherFilter,
    )
    search_fields = django.contrib.auth.admin.UserAdmin.search_fields + ("teams__name",)
    fieldsets = django.contrib.auth.admin.UserAdmin.fieldsets + (
        (
            "Genepy",
            {
                "fields": (
                    "points",
                    "public_profile",
                    "show_in_leaderboard",
                    "html_teams",
                )
            },
        ),
    )
    readonly_fields = ("html_teams",)
    widgets = ()

    @admin.display(description="points")
    def formatted_points(self, obj):
        return f"{obj.points:.2f}"

    def html_teams(self, obj):
        return format_html(
            ", ".join(
                [
                    f"<a href={reverse('admin:hkis_team_change', args=(m.team.pk,))}>"
                    + escape(m.team.name)
                    + "</a>"
                    for m in obj.user.memberships.all()
                ]
            )
        )


admin.site.register(User, UserAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Exercise, ExerciseAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Page, PageAdmin)

urlpatterns = [
    path("email-test", EmailTestView.as_view()),
    path("error-test", ErrorTestView.as_view()),
    path("correction-bots", CorrectionBotsView.as_view()),
    path("debug-toolbar", DebugToolbarView.as_view()),
]
