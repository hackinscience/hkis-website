from collections import defaultdict
from contextlib import suppress

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from rest_framework.reverse import reverse as api_reverse

from hkis.forms import AnswerForm
from hkis.models import Answer, Exercise, Membership, Page, Tag, Team, User


def get_exercises_tags(page=None) -> tuple[list[Exercise], list[Tag]]:
    exercises = (
        Exercise.objects.filter(is_published=True)
        .select_related("author", "page")
        .prefetch_related("tags")
        .only(
            "title_fr",
            "title_en",
            "author",
            "solved_by",
            "page__slug",
            "position",
            "slug",
            "author__username",
        )
        .order_by("position")
    )
    if page is not None:
        exercises = exercises.filter(page=page)
    tags: dict[int, Tag] = {}
    for exercise in exercises:
        for tag in exercise.tags.all():
            tag = tags.setdefault(tag.pk, tag)  # Deduplicate tag objects
            try:
                tag.cached_exercises.append(exercise)
                tag.nb_exercises += 1
            except AttributeError:
                tag.cached_exercises = [exercise]
                tag.nb_exercises = 1
    list_of_tags = sorted(tags.values(), key=lambda tag: tag.position)
    for tag in list_of_tags:
        tag.cached_exercises.sort(key=lambda e: e.position)
    return exercises, list_of_tags


def add_solved_by_tags(user, tags: list[Tag]) -> None:
    if user.is_anonymous:
        for tag in tags:
            tag.solved = 0
        return

    done = set(
        Answer.objects.filter(user=user, is_valid=True)
        .distinct()
        .values_list("exercise_id", flat=True)
    )
    for tag in tags:
        tag.solved = sum(1 for exercise in tag.cached_exercises if exercise.pk in done)


def index(request):
    exercises, tags = get_exercises_tags()
    add_solved_by_tags(request.user, tags)
    return render(
        request,
        "hkis/index.html",
        {
            "exercises": len(exercises),
            "tags": tags,
            "answers": Answer.objects.filter(is_shared=True).only("id"),
            "individual_leaders": (
                User.with_rank.filter(rank__isnull=False).only(
                    "first_name", "last_name", "username", "points"
                )[:3]
            ),
            "team_leaders": Team.objects.order_by("-points")[:3],
        },
    )


class ProfileView(UpdateView):
    model = User
    fields = ["username", "email"]
    template_name = "hkis/profile.html"
    context_object_name = "profile"

    def __init__(self, **kwargs):
        self.object = None
        super().__init__(**kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.kwargs.get("username") != self.object.username:
            return redirect("profile", self.object.username)
        return super().get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset.filter(username=self.kwargs.get("username")).get()
        except queryset.model.DoesNotExist:
            try:
                return queryset.filter(pk=int(self.kwargs.get("username"))).get()
            except (queryset.model.DoesNotExist, ValueError) as err:
                raise Http404(
                    _("No %(verbose_name)s found matching the query")
                    % {"verbose_name": queryset.model._meta.verbose_name}
                ) from err

    def post(self, request, *args, **kwargs):
        """Everyone can see a profile, but only owner can modify it."""
        if request.user.pk != self.get_object().pk:
            raise PermissionDenied
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        """Here we're getting the real username from the database.

        So if the form was valid and saved we may get a new username.
        And if the form was rejected we're sure to get the old username.
        """
        messages.info(self.request, "Profile updated")
        return reverse(
            "profile", kwargs={"username": User.objects.get(pk=self.object.pk).username}
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = context["object"]
        context["is_me"] = context["object"].pk == self.request.user.pk
        context["user_info"] = User.with_rank.get(pk=user.pk)
        context["memberships"] = user.memberships.select_related("team")
        if not context["is_me"]:
            context["memberships"] = context["memberships"].filter(team__is_public=True)
        context["done_qty"] = (
            Answer.objects.filter(user=user, is_valid=True)
            .values_list("exercise_id")
            .distinct()
            .count()
        )
        context["submit_qty"] = Answer.objects.filter(user=user).count()
        context["languages"] = settings.LANGUAGES
        context["api_current_user_url"] = api_reverse(
            "user-detail", args=[user.pk], request=self.request
        )
        rank = context["user_info"].rank
        if context["user_info"].rank is not None:
            context["leaderboard"] = (
                User.with_rank.order_by("rank")
                .filter(rank__isnull=False)
                .prefetch_related("teams")[max(0, rank - 1 - 5) : rank - 1 + 5]
            )
        context["teams"] = Team.objects.order_by("name")

        return context


def old_profile_view(request, pk):
    user = User.objects.get(pk=pk)
    return redirect("profile", username=user.username)


class Leaderboard(ListView):
    queryset = User.with_rank.filter(rank__isnull=False).prefetch_related("teams")
    paginate_by = 100
    template_name = "hkis/leaderboard.html"
    ordering = ["-points"]


class TeamLeaderboard(ListView):
    queryset = Team.objects.filter(is_public=True).prefetch_related("members")
    paginate_by = 100
    template_name = "hkis/teams.html"
    ordering = ["-points"]


class ExercisesView(DetailView):
    model = Page
    slug_url_kwarg = "exercises"
    template_name = "hkis/exercises.html"

    def flag_by_difficulty(self, exercises):
        by_solved = sorted(exercises, key=lambda e: e.solved_by)
        pct = len(by_solved) / 100
        easy = int(25 * pct)
        medium = int(25 * pct + 40 * pct)
        hard = int(25 * pct + 40 * pct + 25 * pct)
        for exercise in exercises[:easy]:
            exercise.difficulty = "easy"
        for exercise in exercises[easy:medium]:
            exercise.difficulty = "medium"
        for exercise in exercises[medium:hard]:
            exercise.difficulty = "hard"
        for exercise in exercises[hard:]:
            exercise.difficulty = "very-hard"

    def get_exercises_done(self, selected_tag: Tag | None, page: Page):
        request = self.request
        done = Answer.objects.filter(
            user=request.user, is_valid=True, exercise__page=page
        ).distinct()
        failed = Answer.objects.filter(
            user=request.user, is_valid=False, exercise__page=page
        ).distinct()
        if selected_tag is not None:
            done = done.filter(exercise__tags=selected_tag)
            failed = failed.filter(exercise__tags=selected_tag)
        done = {row[0] for row in done.values_list("exercise_id")}
        failed = {row[0] for row in failed.values_list("exercise_id")}
        return done, failed

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data()
        request = self.request
        resolved = None
        with suppress(ValueError):
            resolved = int(request.GET.get("resolved", ""))
        page = context["object"]
        exercises, tag_list = get_exercises_tags(page)
        tags = {tag.slug: tag for tag in tag_list}
        self.flag_by_difficulty(exercises)
        if selected_tag := tags.get(request.GET.get("tag", "")):
            exercises = [e for e in exercises if selected_tag in e.tags.all()]
            context["title"] = selected_tag.title
            context["body"] = selected_tag.description
        else:
            selected_tag = None
            context["title"] = page.title
            context["body"] = page.body
        if not request.user.is_anonymous:
            done, failed = self.get_exercises_done(selected_tag, page)
            todo = {exercise.id for exercise in exercises if exercise.id not in done}
            next_exercise = next((e for e in exercises if e.id not in done), None)
        else:
            done = set()
            failed = set()
            todo = {exercise.id for exercise in exercises}
            next_exercise = next(iter(exercises), None)
        if resolved is not None:
            if resolved:
                exercises = [e for e in exercises if e.id in done]
            else:
                exercises = [e for e in exercises if e.id in todo]
        context["total"] = len(exercises)
        return context | {
            "resolved": resolved,
            "exercises_done": done,
            "exercises_todo": todo,
            "exercises_failed": failed,
            "next_exercise": next_exercise,
            "selected_tag": selected_tag,
            "exercises": exercises,
            "tags": tags.values(),
        }


class ExerciseView(DetailView):
    model = Exercise
    template_name = "hkis/exercise.html"

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset.get(
                page__slug=self.kwargs["exercises"], slug=self.kwargs["exercise"]
            )
        except queryset.model.DoesNotExist as err:
            raise Http404("No exercise found matching the query") from err

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.select_related("author")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["LANGUAGE_CODE"] = self.request.LANGUAGE_CODE
        user = self.request.user
        last_answer = None
        if not user.is_anonymous:
            last_answer = self.object.answers.filter(user=user).order_by("-id").first()
        context["answer_form"] = AnswerForm(
            language=self.object.page.language,
            initial={
                "exercise": f"/api/exercises/{self.object.id}/",
                "source_code": (
                    last_answer.source_code
                    if last_answer
                    else context["exercise"].initial_solution
                ),
            },
        )
        try:
            context["current_rank"] = User.with_rank.get(pk=user.pk).rank
        except (User.DoesNotExist, TypeError):
            context["current_rank"] = 999_999
        if user.is_anonymous:
            context["is_valid"] = False
        else:
            context["is_valid"] = bool(
                self.object.answers.filter(user=user, is_valid=True)
            )
        context["next"] = Exercise.objects.filter(
            page=self.object.page,
            is_published=True,
            position__gt=self.object.position,
        ).order_by("position")
        context["previous"] = Exercise.objects.filter(
            is_published=True,
            page=self.object.page,
            position__lt=self.object.position,
        ).order_by("-position")
        return context


class SolutionView(LoginRequiredMixin, DetailView):
    model = Exercise
    template_name = "hkis/solutions.html"

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset.get(
                page__slug=self.kwargs["page"], slug=self.kwargs["exercise"]
            )
        except queryset.model.DoesNotExist as err:
            raise Http404("No exercise found matching the query") from err

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["my_answers"] = self.object.answers.filter(
            user=self.request.user
        ).order_by("created_at")
        if self.object.is_solved_by(self.request.user):
            context["is_solved"] = True
            context["solutions"] = self.object.shared_solutions()[:10]
        else:
            context["is_solved"] = False
            context["solutions"] = []
        return context


def team_stats(request, slug):
    try:
        team = Team.objects.get(slug=slug)
    except Team.DoesNotExist as err:
        raise Http404("Team does not exist") from err

    requester_membership = None
    if not request.user.is_anonymous:
        with suppress(Membership.DoesNotExist):
            requester_membership = Membership.objects.get(team=team, user=request.user)
    if requester_membership is None:
        return HttpResponseForbidden("You're not in that team")
    if requester_membership.role != Membership.Role.STAFF:
        return HttpResponseForbidden("You're not staff in that team")

    users = []
    by_user = defaultdict(dict)
    exercises = {}

    for user in User.objects.filter(teams=team).order_by("-points"):
        users.append(user)
        for exercise in Exercise.objects.with_user_stats(user):
            exercises[exercise.pk] = exercise
            by_user[user.pk][exercise.pk] = exercise

    exercises = sorted(exercises.values(), key=lambda exercise: exercise.position)

    table = []  # Table is one line per user, one column per exercise.
    for user in users:
        line = []
        for exercise in exercises:
            line.append(by_user[user.pk].get(exercise.pk))
        table.append(line)

    context = {
        "table": zip(users, table),
        "exercises": exercises,
        "team": team,
    }
    return render(request, "hkis/team_stats.html", context)


def team_leaderboard_redirect(request):
    return redirect("teams")


def team_view(request, slug):
    memberships = (
        Membership.objects.select_related("user", "team")
        .filter(team__slug=slug)
        .exclude(role=Membership.Role.PENDING)
        .order_by("-user__points")
    )
    pending_memberships = (
        Membership.objects.select_related("user", "team")
        .filter(team__slug=slug)
        .filter(role=Membership.Role.PENDING)
    )
    requester_membership = None
    for membership in memberships:
        if membership.user_id == request.user.pk:
            requester_membership = membership
    try:  # Try to get the team info from the select_releated
        team = memberships.first().team
    except AttributeError:  # OK OK let's query the DB then.
        try:
            team = Team.objects.get(slug=slug)
        except Team.DoesNotExist as err:
            raise Http404("Team does not exist") from err
    context = {
        "memberships": memberships,
        "pending_memberships": pending_memberships,
        "team": team,
        "requester_membership": requester_membership,
    }
    return render(request, "hkis/team.html", context)


def stats(request):
    my_exercises = Exercise.objects.with_monthly_stats(author=request.user)
    context = {"exercises": my_exercises}
    return render(request, "hkis/stats.html", context)
