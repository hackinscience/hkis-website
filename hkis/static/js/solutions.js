function sharedAnswer(answerId, isShared, csrfToken) {
    var data = {
        "is_shared": isShared,
    };
    var xhr = new XMLHttpRequest();
    xhr.open("PATCH", `/api/answers/${answerId}/`, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("X-CSRFToken", csrfToken);
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById(`btn-${answerId}-share`).style.display = isShared ? "none" : "";
            document.getElementById(`btn-${answerId}-unshare`).style.display = isShared ? "" : "none";
        }
    };
    xhr.send(JSON.stringify(data));
}

function deleteAnswer(answerId, csrfToken) {
    if (!confirm(gettext("Are you sure you want to delete this answer?")))
        return;
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", `/api/answers/${answerId}/`, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("X-CSRFToken", csrfToken);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 204) {
            document.getElementById(`btn-${answerId}-delete`).style.display = "none";
        }
    }
    xhr.send();
}

function selectSolution(select) {
    select.parentNode.querySelectorAll(".solution").forEach(solution => {
        solution.classList.add("disabled");
    });
    document.getElementById(select.value).classList.remove("disabled");
}

window.addEventListener("DOMContentLoaded", e => {
    settings = document.getElementsByTagName('body')[0].dataset;

    document.querySelectorAll("button[data-share]").forEach(button => {
        button.addEventListener("click", e => {
            sharedAnswer(button.dataset.share, true, settings.csrfToken);
        });
    });
    document.querySelectorAll("button[data-unshare]").forEach(button => {
        button.addEventListener("click", e => {
            sharedAnswer(button.dataset.unshare, false, settings.csrfToken);
        });
    });
    document.querySelectorAll("button[data-delete]").forEach(button => {
        button.addEventListener("click", e => {
            deleteAnswer(button.dataset.delete, settings.csrfToken);
        });
    });

    document.querySelectorAll(".solution-selector").forEach(select => {
        selectSolution(select);
        select.addEventListener("change", e => {
            selectSolution(select);
        });
    });
});
