import datetime as dt
import json
import logging
import statistics
import sys

import django.contrib.auth.models
from django.contrib import admin
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import IntegrityError, models
from django.db.models import Count, F, Min, Q, Sum, Value
from django.db.models.expressions import Window
from django.db.models.functions import DenseRank, NullIf
from django.db.models.signals import post_save
from django.db.models.sql.constants import LOUTER
from django.dispatch import receiver
from django.template.defaultfilters import truncatechars
from django.urls import reverse
from django.utils.text import Truncator
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django_cte import CTEManager, CTEQuerySet, With
from django_extensions.db.fields import AutoSlugField
from rest_framework.reverse import reverse as api_reverse

logger = logging.getLogger(__name__)


class UserCTEQuerySet(CTEQuerySet):
    """QuerySet attached to the User.with_rank manager."""

    def with_rank(self):
        """Use a Common Table Expression to add rank to Users.

        The resulting query looks like:

            WITH cte AS (
                SELECT *, DENSE_RANK() OVER (ORDER BY hkis_userinfo.points DESC) AS r
                FROM "hkis_userinfo")
            SELECT * FROM cte

        The idea is with_rank() can be chained with filters without
        modifying the window, generating queries like:

            WITH cte AS (
                SELECT *, DENSE_RANK() OVER (ORDER BY hkis_userinfo.points DESC) AS r
                FROM "hkis_userinfo")
            SELECT * FROM cte
            WHERE ...

        Without a CTE,
        `User.with_rank.filter(user__username="anyone")`
        would always tell the user is ranked 1st (as the only one in its selection).
        """
        with_rank = With(
            self.annotate(
                rank=Window(order_by=F("points").desc(), expression=DenseRank())
            ).filter(show_in_leaderboard=True)
        )
        return (
            with_rank.join(User, pk=with_rank.col.pk, _join_type=LOUTER)
            .with_cte(with_rank)
            .annotate(rank=with_rank.col.rank)
        )


class UserQuerySet(CTEQuerySet):
    def recompute_points(self):
        """Recompute all user points, usefull after updating the points given
        by an exercise.
        """
        for userinfo in User.objects.all():
            userinfo.recompute_points()


class UserManager(django.contrib.auth.models.UserManager, CTEManager):
    def recompute_points(self):
        """Recompute all user points, usefull after updating the points given
        by an exercise.
        """
        for user in User.objects.all():
            user.recompute_points()


class WithRankUserManager(CTEManager):
    """User.with_rank manager, to get:

    User.with_rank.first().rank

    ⚠ Users with show_in_leaderboard=False are **excluded** from this manager.
    ⚠ It also mean  it can't be used as a base_manager!
    """

    def get_queryset(self):
        return UserCTEQuerySet(self.model, using=self._db).with_rank()


class User(AbstractUser):
    class Meta:
        db_table = "auth_user"
        indexes = (
            models.Index(fields=["email"]),
            models.Index(fields=["-points"]),
            models.Index(fields=["show_in_leaderboard", "-points"]),
        )

    objects = UserManager()
    with_rank = WithRankUserManager.from_queryset(UserCTEQuerySet)()
    points = models.FloatField(default=0)  # Computed sum of solved exercise positions.
    public_profile = models.BooleanField(default=True)
    show_in_leaderboard = models.BooleanField(default=True)

    def public_teams(self):
        return self.teams.filter(is_public=True)

    def recompute_points(self) -> None:
        """Reconpute the number of points for this user.

        Points for one exercise done:

        - Equals to the position of the exercise, itself often equal
          to number_of_solves_of_easiest_exercise - number_of_solve

        - Solving it "late" remove points, but doing more exercise
          should always grant more than doing them first!

        - Only less than one point can be lost by doing it late, so it
          won't appear visually when ceil()ed, but make 1st solver 1st
          in the leaderboard.
        """
        points = 0
        for exercise in Exercise.objects.with_user_stats(user=self).only(
            "points", "created_at"
        ):
            if exercise.user_successes:
                time_to_solve = (
                    max(0, (exercise.solved_at - exercise.created_at).total_seconds())
                    + sys.float_info.epsilon
                )
                points += exercise.points - (time_to_solve**0.0001 - 1)
        self.points = points
        self.save()

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"
        return self.username


class PageQuerySet(models.QuerySet):
    def editable_by(self, user):
        if user.is_superuser:
            return self
        return self.filter(author=user)


class Page(models.Model):
    objects = PageQuerySet.as_manager()
    author = models.ForeignKey(
        User, verbose_name=_("author"), on_delete=models.SET_NULL, blank=True, null=True
    )
    slug = AutoSlugField(populate_from=["title_en"], editable=False)
    title = models.CharField(_("title"), max_length=512)
    body = models.TextField(_("content"), default="", blank=True)
    position = models.FloatField(default=0, blank=True)
    in_menu = models.BooleanField(
        verbose_name=_("in menu"),
        help_text=_(
            "Check this box to have this page appear publicly in the header."
            " The first page (lowest 'position') of the menu is considered the home page"
            " (all users land to it)."
        ),
        default=False,
        blank=True,
    )
    language = models.CharField(
        max_length=25,
        help_text=_(
            "Default language for exercises of this page, "
            "used by the code widget for syntax highlighting."
        ),
        default="python",
    )

    def get_absolute_url(self):
        return reverse("exercises", args=[self.slug])

    def get_api_url(self):
        return reverse("page-detail", args=[self.pk])

    def __str__(self):
        return self.title


class Tag(models.Model):
    class Meta:
        ordering = ("position",)
        verbose_name_plural = "Tags"

    title = models.CharField(_("title"), max_length=255)
    description = models.TextField(blank=True, default="")
    slug = AutoSlugField(
        populate_from=["title_en"], editable=True, overwrite_on_add=False
    )
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    position = models.FloatField(default=0)

    def __str__(self):
        return self.title or self.title_en or "Unnamed"  # pylint: disable=no-member


class ExerciseQuerySet(models.QuerySet):
    def reorganize(self, save=True):
        """Reorganize exercise by number of solves.

        Returns a list of (new_position, exercise) tuples.
        """
        all_exercises = self.with_last_month_successes().order_by(
            "-last_month_successes"
        )
        by_position = []
        for position, exercise in enumerate(all_exercises, start=1):
            by_position.append((position, exercise))
            if save:
                exercise.position = position
                exercise.save()
        return by_position

    def with_successes(self):
        return self.annotate(
            successes=Count(
                "answers__user", filter=Q(answers__is_valid=True), distinct=True
            ),
        )

    def with_last_month_successes(self):
        return self.annotate(
            last_month_successes=Count(
                "answers__user",
                filter=Q(
                    answers__is_valid=True,
                    answers__created_at__gt=now() - dt.timedelta(days=30),
                ),
                distinct=True,
            ),
        )

    def with_user_stats(self, user):
        if user.is_anonymous:
            return self.annotate(
                solved_at=Value(now(), models.DateTimeField()),
                user_successes=Value(0, models.IntegerField()),
            )
        return self.filter(answers__user=user).annotate(
            solved_at=Min("answers__created_at"),
            user_successes=Count("answers", filter=Q(answers__is_valid=True)),
            user_tries=Count("answers"),
        )

    def with_monthly_stats(self, author):
        return (
            self.filter(author=author.pk)
            .order_by("position")
            .annotate(
                tries=Count(
                    "answers__user",
                    filter=Q(
                        answers__created_at__gt=now() - dt.timedelta(days=30),
                        answers__user__is_staff=False,
                    ),
                    distinct=True,
                ),
                prev_tries=Count(
                    "answers__user",
                    filter=Q(
                        answers__created_at__gt=now() - dt.timedelta(days=60),
                        answers__created_at__lt=now() - dt.timedelta(days=30),
                        answers__user__is_staff=False,
                    ),
                    distinct=True,
                ),
                tries_difference=F("tries") - F("prev_tries"),
                successes=Count(
                    "answers__user",
                    filter=Q(
                        answers__is_valid=True,
                        answers__user__is_staff=False,
                        answers__created_at__gt=now() - dt.timedelta(days=30),
                    ),
                    distinct=True,
                ),
                prev_successes=Count(
                    "answers__user",
                    filter=Q(
                        answers__is_valid=True,
                        answers__user__is_staff=False,
                        answers__created_at__gt=now() - dt.timedelta(days=60),
                        answers__created_at__lt=now() - dt.timedelta(days=30),
                    ),
                    distinct=True,
                ),
                successes_difference=F("successes") - F("prev_successes"),
                success_ratio=100 * F("successes") / NullIf(F("tries"), 0),
                prev_success_ratio=100
                * F("prev_successes")
                / NullIf(F("prev_tries"), 0),
                success_ratio_difference=F("success_ratio") - F("prev_success_ratio"),
            )
        )

    def recompute_solved_by(self):
        for exercise in self.with_successes():
            exercise.solved_by = exercise.successes
            exercise.save()


class Exercise(models.Model):
    class Meta:
        ordering = ("position",)
        constraints = [
            # Per page, exercies have a unique slug because
            # exercises URLs looks like /{page_slug}/{exercise_slug}/
            models.UniqueConstraint(fields=["page", "slug"], name="unique_page_slug"),
        ]

    title = models.CharField(max_length=255)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    slug = AutoSlugField(
        populate_from=["title_en"], editable=True, allow_duplicates=True
    )
    # check is ran inside the sandbox, in a `check.py` file, near a
    # `solution` file containing the student code.
    # check.py has the LANGUAGE env set to the user
    # preferences,
    check_py = models.TextField(blank=True, default="")
    is_published = models.BooleanField(default=False)
    wording = models.TextField(blank=True, default="")
    initial_solution = models.TextField(blank=True, default="")
    position = models.FloatField(default=0)
    objects = ExerciseQuerySet.as_manager()
    created_at = models.DateTimeField(auto_now_add=True)
    # Number of points are granted for solving this exercise
    points = models.IntegerField(default=1)
    tags = models.ManyToManyField(Tag, related_name="exercises", blank=True)
    page = models.ForeignKey(Page, on_delete=models.RESTRICT, related_name="exercises")
    # Number of users successfully solving the exercise.
    # This could, or not, count solves by non-logged users
    # I did not made my mind yet.
    solved_by = models.IntegerField(default=0)

    def description(self):
        """The first non-title paragraph of the Markdown wording."""
        for paragraph in self.wording.split("\n\n"):
            if paragraph.startswith("#"):
                continue
            return paragraph.replace("\n", " ")

    def shared_solutions(self):
        return Answer.objects.filter(
            exercise=self, is_valid=True, is_shared=True
        ).order_by("-user__points")

    def is_solved_by(self, user):
        return self.answers.filter(user=user, is_valid=True).exists()

    def clean(self):
        """Clean windows-style newlines, maybe inserted by Ace editor, or
        other users.
        """
        self.check_py = self.check_py.replace("\r\n", "\n")
        self.wording = self.wording.replace("\r\n", "\n")
        if self.is_published:
            if not self.check_py:
                raise ValidationError(
                    _("Exercises should have a check script in order to be published.")
                )
            if not self.wording:
                raise ValidationError(
                    _("Exercises should have a wording in order to be published.")
                )

    def get_absolute_url(self):
        return reverse("exercise", args=[self.page.slug, self.slug])

    def get_solutions_absolute_url(self):
        return reverse("solutions", args=[self.page.slug, self.slug])

    def get_api_url(self):
        return reverse("exercise-detail", args=[self.pk])

    def __str__(self):
        return f"{self.page.title} — {self.title}"


class AnswerQuerySet(models.QuerySet):
    def visible_by(self, user):
        qs = self.select_related("user", "exercise")
        if user.is_superuser:
            return qs
        my_students = {student.pk for student in Team.objects.my_students(user)}
        return qs.filter(user__in=my_students | {user.pk})


class Answer(models.Model):
    class Meta:
        verbose_name = _("answer")
        indexes = [
            models.Index(fields=["exercise", "-votes"]),
            models.Index(
                fields=["is_valid", "user"],
                include=["exercise"],
                name="is_valid__user",
            ),
            models.Index(fields=["exercise", "is_valid", "is_shared"]),
            models.Index(fields=["exercise", "user", "is_valid"]),
        ]

    objects = AnswerQuerySet.as_manager()

    exercise = models.ForeignKey(
        Exercise,
        on_delete=models.CASCADE,
        related_name="answers",
        verbose_name=_("exercise"),
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        editable=False,
        null=True,
        blank=True,
        verbose_name=_("user"),
    )
    source_code = models.TextField(blank=True)
    is_corrected = models.BooleanField(default=False, verbose_name=_("corrected"))
    is_valid = models.BooleanField(default=False, verbose_name=_("valid"))
    is_shared = models.BooleanField(default=False, verbose_name=_("shared"))
    correction_message = models.TextField(default="", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    corrected_at = models.DateTimeField(blank=True, null=True)
    is_unhelpfull = models.BooleanField(
        default=False, blank=True, verbose_name=_("unhelpfull")
    )
    votes = models.IntegerField(default=0, blank=True, null=False)  # Sum of Vote.value

    @admin.display(description=_("correction message"))
    def short_correction_message(self):
        """Extract a single, hopefully interesting, line from a correction message."""
        for line in self.correction_message.splitlines():
            if line.startswith("!!! "):  # an admonition, like '!!! info ""'
                continue
            if line.strip() == "":
                continue
            return truncatechars(line, 100)
        return ""

    def __str__(self):
        username = Truncator(self.user.username if self.user else "Anon").chars(30)
        return f"{username} on {self.exercise.title}"

    def get_absolute_url(self):
        return self.exercise.get_absolute_url()

    def save(self, *args, **kwargs):
        if self.correction_message and self.correction_message.startswith("Traceback"):
            self.is_unhelpfull = True
        super().save(*args, **kwargs)

    def send_to_correction_bot(self, lang="en"):
        raise NotImplementedError("TODO: à réimplémenter avec Redis.")
        # sync_check_answer = async_to_sync(check_answer)
        # is_valid, message = sync_check_answer(
        #     {
        #         "check": self.exercise.check_py,
        #         "source_code": self.source_code,
        #         "language": lang,
        #     }
        # )
        # self.correction_message = message
        # self.is_corrected = True
        # self.is_valid = is_valid
        # self.corrected_at = now()
        # self.save()


class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    value = models.IntegerField(null=False)  # Typically +1 or -1


@receiver(post_save, sender=Vote)
def cb_update_vote_count(sender, instance, **kwargs):
    instance.answer.votes = Vote.objects.filter(answer=instance.answer).aggregate(
        Sum("value")
    )["value__sum"]
    instance.answer.save()


class TeamQuerySet(models.QuerySet):
    def my_teams(self, user):
        """Get teams for which user is staff."""
        return self.filter(
            Q(membership__role=Membership.Role.STAFF) & Q(membership__user=user)
        )

    def my_students(self, user):
        """Get members from teams for which user is staff."""
        my_teams = self.my_teams(user)
        return User.objects.filter(memberships__team__in=my_teams)

    def is_my_student(self, teacher, student):
        return student in self.my_students(teacher)

    def recompute_ranks(self):
        for team in Team.objects.all():
            team.recompute_rank()

    def teams_i_can_see(self, user):
        """Give a queryset of teams that the given user is allowed to see.

        One can see public teams plus its teams.
        """
        if user.is_superuser:
            return self.all()
        if user.is_anonymous:
            return self.filter(is_public=True)
        return self.filter(
            Q(is_public=True)
            | (
                Q(membership__user=user)
                & (
                    Q(membership__role=Membership.Role.STAFF)
                    | Q(membership__role=Membership.Role.MEMBER)
                )
            )
        ).distinct()


class Team(models.Model):
    class Meta:
        verbose_name = _("team")

    objects = TeamQuerySet.as_manager()
    name = models.CharField(max_length=42, unique=True)
    slug = AutoSlugField(populate_from=["name"], unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    members = models.ManyToManyField(User, through="Membership", related_name="teams")
    is_public = models.BooleanField(default=True)
    points = models.FloatField(default=0)  # Computed value trying to represent the team

    def recompute_rank(self):
        """Try to mix member score to get a representative team score."""
        values = 0
        weights = 1  # Won't change much big teams, but penalize too-small teams.
        i = 1
        for member in self.membership_set.filter(
            Q(role=Membership.Role.STAFF) | Q(role=Membership.Role.MEMBER)
        ).order_by("user__points"):
            values += member.user.points * i
            weights += i
            i += i
        try:
            self.points = values / weights
        except ZeroDivisionError:
            self.points = 0
        self.save()

    def is_staff(self, user):
        if user.is_anonymous:
            return False
        return self.membership_set.filter(
            user=user, role=Membership.Role.STAFF
        ).exists()

    def add_member(self, username):
        """Join a team.

        If the team has no staff yet, join as staff, else join as
        pending member.
        """
        try:
            membership = Membership.objects.create(
                team=self,
                user=User.objects.get(username=username),
                role=Membership.Role.PENDING,
            )
            self.ensure_has_staff_or_drop_team()
            return membership
        except IntegrityError:  # Already member of the team.
            return None

    def ensure_has_staff_or_drop_team(self):
        """To be called when a user leaves a team.

        - If the team has no longer a staff: elect one.
        - If the team is empty: remove it.
        """
        # If there's no more staff, pick oldest member as staff:
        if self.membership_set.filter(role=Membership.Role.STAFF):
            return  # OK there's still one staff :)

        for membership in self.membership_set.filter(
            role=Membership.Role.MEMBER
        ).order_by("-created_at"):
            membership.role = Membership.Role.STAFF
            membership.save()
            return
        # No member to grant?
        # Last resort: Pick a pending member as new staff:
        for membership in self.membership_set.order_by("-created_at"):
            membership.role = Membership.Role.STAFF
            membership.save()
            return
        # No member left at all? Drop the team.
        self.delete()

    def remove_member(self, username):
        """Remove a member from the team.

        If no staff remain after removal, elect a new staff.

        If no member left after removal, remove the team.
        """
        self.membership_set.filter(user__username=username).delete()
        self.ensure_has_staff_or_drop_team()

    def accept(self, username):
        membership = self.membership_set.get(user__username=username)
        membership.role = Membership.Role.MEMBER
        membership.save()

    def members_by_rank(self):
        return self.membership_set.order_by("-user__points")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("team", args=[self.slug])

    @property
    def pending_qty(self):
        return self.membership_set.filter(role=Membership.Role.PENDING).count()


class CorrectionBotStats:
    def __init__(self, bot_name, json_data: bytes):
        self.name = bot_name.decode("UTF-8")
        data = json.loads(json_data.decode("ASCII"))
        self.uptime = dt.timedelta(seconds=data["uptime_seconds"])
        self.last_time_i_worked = dt.datetime.fromtimestamp(
            data["last_time_i_worked"] or 0, tz=dt.UTC
        )
        self.number_of_exercises_corrected = data["number_of_exercises_corrected"]
        working = [delay["time_spent_working"] for delay in data["delays"]]
        waiting = [delay["time_spent_waiting"] for delay in data["delays"]]
        self.median_work_duration = (
            dt.timedelta(seconds=statistics.median(working)) if working else None
        )
        self.median_wait_duration = (
            dt.timedelta(seconds=statistics.median(waiting)) if working else None
        )


class Membership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="memberships")
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [["user", "team"]]

    class Role(models.TextChoices):
        PENDING = "PE", _("Pending")
        MEMBER = "MM", _("Member")
        STAFF = "ST", _("Staff")

    def __str__(self):
        return f"{self.user.username} in {self.team.name}"

    def api_url(self):
        return api_reverse("membership-detail", args=(self.pk,), request=None)

    role = models.CharField(
        max_length=2,
        choices=Role.choices,
        default=Role.PENDING,
    )
