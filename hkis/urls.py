from django.shortcuts import redirect
from django.urls import include, path
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog

from hkis import api, views

urlpatterns = [
    path("", views.index, name="index"),
    path(
        "team/",
        TemplateView.as_view(template_name="hkis/genepy-team.html"),
        name="genepy-team",
    ),
    path("help/", TemplateView.as_view(template_name="hkis/help.html"), name="help"),
    path("about/", TemplateView.as_view(template_name="hkis/about.html"), name="about"),
    path(
        "sponsor/",
        TemplateView.as_view(template_name="hkis/sponsor.html"),
        name="sponsor",
    ),
    path("legal/", TemplateView.as_view(template_name="hkis/legal.html"), name="legal"),
    path(
        "privacy/",
        TemplateView.as_view(template_name="hkis/privacy.html"),
        name="privacy",
    ),
    path("jsi18n/", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    path("teams/<slug:slug>", views.team_view, name="team"),
    path("teams/<slug:slug>/stats", views.team_stats, name="team-stats"),
    path("api/", include(api.urls)),
    path("api-auth/", include("rest_framework.urls")),
    path("profile/<int:pk>", views.old_profile_view, name="old_profile"),
    path("user/<username>", views.ProfileView.as_view(), name="profile"),
    path("leaderboard/", views.Leaderboard.as_view(), name="leaderboard"),
    path("teams/", views.TeamLeaderboard.as_view(), name="teams"),
    path("team-leaderboard/", views.team_leaderboard_redirect),
    path("stats/", views.stats, name="stats"),
    path("<slug:exercises>/", views.ExercisesView.as_view(), name="exercises"),
    path(
        "<slug:exercises>/<slug:exercise>",
        views.ExerciseView.as_view(),
        name="exercise",
    ),
    path(
        "<slug:page>/<slug:exercise>/solutions",
        views.SolutionView.as_view(),
        name="solutions",
    ),
    path(
        "favicon.ico",
        lambda request: redirect("/static/img/favicon.svg", permanent=True),
    ),
]
