import asyncio
import json
import logging
from typing import Optional
from uuid import uuid4

import redis.asyncio as redis
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.conf import settings
from django.utils.timezone import now

from hkis.models import Answer, Exercise, User
from hkis.serializers import AnswerSerializer
from hkis.utils import markdown_to_bootstrap

logger = logging.getLogger(__name__)

redis_client = redis.Redis()

# Channels reminders:
# - The consumer class is instanciated once per websocket connection
#   (once per browser tab), it's the lifespan of a what channels call a scope.
# - Group can group together multiple scopes, usefull to send a
#   message to all browser tabs of a given user at once for example.


def serialize_answer(answer: Answer, rank: Optional[int] = None) -> dict:
    """Serialize a given answer for transmisison over websocket.

    It uses DRF Serializer so it'll look like what the API would give.
    """
    message = AnswerSerializer(answer, context={"request": None}).data
    if rank:
        message["user_rank"] = rank
    message["correction_message_html"] = markdown_to_bootstrap(
        message["correction_message"]
    )
    message["type"] = "answer.update"
    return message


class ExerciseConsumer(AsyncJsonWebsocketConsumer):
    """Read and write from Webscoket, mainly forwareding to Redis PUBSUB.

    Behind Redis PUBSUB are the correction bots.
    """

    def __init__(self, *args, **kwargs):
        self.settings = {}
        self.exercise: Optional[Exercise] = None
        self.redis_reader_task = None
        super().__init__(*args, **kwargs)
        self.redis_channel = uuid4().hex
        self.pubsub = None

    def log(self, message, *args):
        chunks = []
        if self.scope["user"]:
            chunks.append(str(self.scope["user"]))
        if self.exercise:
            chunks.append(self.exercise.slug)
        chunks.append(message)
        if args:
            chunks.append(": " + str(args))
        logger.info("%s", " ".join(chunks))

    async def connect(self):
        self.log("connecting")
        self.exercise = await Exercise.objects.aget(
            pk=self.scope["url_route"]["kwargs"]["exercise_id"]
        )
        await self.accept()
        self.log("connected")

        self.redis_reader_task = asyncio.create_task(self.redis_reader())

    async def disconnect(self, code):
        self.log(f"disconnect (code={code})")
        if self.redis_reader_task:
            self.redis_reader_task.cancel()
            await self.redis_reader_task

    async def receive_json(self, content, **kwargs):
        if content["type"] == "answer":
            await self.receive_answer_from_browser(content["source_code"])
        elif content["type"] == "settings":
            self.settings = content["value"]
        else:
            self.log("Unknown message received", json.dumps(content))

    async def receive_answer_from_browser(self, source_code):
        self.log("Got answer from browser")
        answer = await self.exercise.answers.acreate(
            source_code=source_code, user_id=self.scope["user"].id
        )
        answer_json = {
            "id": answer.pk,
            "check_py": answer.exercise.check_py,
            "source_code": source_code,
            "exercise_slug": answer.exercise.slug,
            "language": self.settings.get("LANGUAGE_CODE", "en"),
            "user": str(self.scope["user"]),
        }
        self.log("Sending answer to correction bots...")
        await self.send_answer_to_correction_bot(answer_json)
        await self.send_json(serialize_answer(answer))

    async def redis_reader(self):
        self.log("Will subscribe to redis", self.redis_channel)
        try:
            async with redis_client.pubsub() as self.pubsub:
                await self.pubsub.subscribe(self.redis_channel)
                self.log("Subscribed to redis", self.redis_channel)
                while True:
                    try:
                        message = await self.pubsub.get_message(
                            ignore_subscribe_messages=True, timeout=10
                        )
                        if message is not None:
                            await self.got_result_from_correction_bot(message["data"])
                    except Exception:
                        logger.exception(
                            "While handing a response from the correction bot."
                        )
        finally:
            self.log("Unsubscribed from redis", self.redis_channel)
            self.pubsub = None

    async def got_result_from_correction_bot(self, data):
        self.log("Got result from correction bot")
        answer = json.loads(data.decode("ASCII"))
        if answer["correction_message"].startswith("Traceback"):
            logger.error(
                "Correction bot crashed, see %s",
                f"https://{getattr(settings, 'INTERNAL_DOMAINS', ['localhost'])[0]}/"
                f"admin/hkis/answer/{answer['id']}/change/",
            )
            answer["correction_message"] = (
                f"```pytb\n{answer['correction_message']}\n```"
            )
        answer = await self.update_answer(answer)
        await self.send_json(serialize_answer(answer))
        self.log("Result sent to user")
        if answer.is_valid and answer.user_id:
            new_rank = await recompute_points(answer.user_id)
            if new_rank:
                await self.send_json({"type": "rank.updated", "new_rank": new_rank})
            await recompute_team_points(answer.user_id)

    async def send_answer_to_correction_bot(self, answer: dict) -> None:
        if self.pubsub is None:
            self.log("Cannot send answer to redis: pubsub is not connected :,-(")
            return
        answer["reply_to"] = self.redis_channel
        await redis_client.rpush(
            "answers", json.dumps(answer, ensure_ascii=True).encode("ASCII")
        )

    async def update_answer(self, answer_dict: dict) -> Answer:
        answer = await Answer.objects.aget(id=answer_dict["id"])
        was_already_solved = await Answer.objects.filter(
            is_valid=True, user_id=answer.user_id, exercise_id=answer.exercise_id
        ).aexists()
        answer.correction_message = answer_dict["correction_message"]
        answer.is_corrected = True
        answer.is_valid = answer_dict["is_valid"]
        answer.corrected_at = now()
        await answer.asave()
        if (
            not was_already_solved
            and answer.is_valid
            and answer.user_id
            and self.exercise
        ):
            # Can I use a transaction asynchronously? async with transaction.atomic()?
            exercise = await Exercise.objects.aget(pk=self.exercise.pk)
            exercise.solved_by += 1
            await exercise.asave()
        return answer


@database_sync_to_async
def recompute_points(user_id) -> int | None:
    user = User.objects.get(pk=user_id)
    user.recompute_points()
    user.save()
    return User.with_rank.get(pk=user_id).rank


@database_sync_to_async
def recompute_team_points(user_id: int) -> None:
    user = User.objects.get(pk=user_id)
    for team in user.teams.all():
        team.recompute_rank()
