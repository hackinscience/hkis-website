import cProfile
from functools import partial
from urllib.parse import urlparse

import bleach
import markdown
from django.conf import settings
from django.contrib.auth.hashers import PBKDF2PasswordHasher
from markdown.extensions.codehilite import CodeHiliteExtension

ALLOWED_TAGS = [
    # Bleach Defaults
    "a",
    "abbr",
    "acronym",
    "b",
    "blockquote",
    "code",
    "em",
    "i",
    "li",
    "ol",
    "strong",
    "ul",
    # Custom Additions
    "br",
    "caption",
    "cite",
    "col",
    "colgroup",
    "dd",
    "del",
    "details",
    "div",
    "dl",
    "dt",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "hr",
    "img",
    "p",
    "pre",
    "span",
    "sub",
    "summary",
    "sup",
    "table",
    "tbody",
    "td",
    "th",
    "thead",
    "tr",
    "tt",
    "kbd",
    "var",
]

ALLOWED_ATTRIBUTES = {
    # Bleach Defaults
    "a": ["href", "title"],
    "abbr": ["title"],
    "acronym": ["title"],
    # Custom Additions
    "*": ["id"],
    "hr": ["class"],
    "img": ["src", "width", "height", "alt", "align", "class"],
    "span": ["class"],
    "div": ["class"],
    "th": ["align"],
    "td": ["align"],
    "code": ["class"],
    "p": ["align", "class"],
}


def _set_target(attrs, new=False):
    if new:
        return None  # Don't create new links.
    try:
        url = urlparse(attrs[(None, "href")])
    except KeyError:
        return attrs
    if url.netloc not in settings.ALLOWED_HOSTS:
        attrs[(None, "target")] = "_blank"
    else:
        attrs.pop((None, "target"), None)
    return attrs


def markdown_to_bootstrap(text):
    """This convert markdown text to html, with two things:
    - Uses bleach.clean to remove unsafe things.
    - Use custom replacements to adapt classes to bootstrap 4
    """

    return (
        bleach.sanitizer.Cleaner(
            tags=getattr(settings, "ALLOWED_TAGS", ALLOWED_TAGS),
            attributes=getattr(settings, "ALLOWED_ATTRIBUTES", ALLOWED_ATTRIBUTES),
            filters=[
                partial(
                    bleach.linkifier.LinkifyFilter,
                    callbacks=[_set_target],
                    skip_tags=["pre"],
                    parse_email=False,
                ),
            ],
        )
        .clean(
            markdown.markdown(
                text,
                extensions=[
                    "fenced_code",
                    CodeHiliteExtension(guess_lang=False),
                    "admonition",
                ],
            ),
        )
        .replace('class="admonition warning"', 'class="alert alert-warning"')
        .replace('class="admonition note"', 'class="alert alert-info"')
        .replace("admonition-title", "alert-heading")
    )


class CProfileMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.profiler = None

    def __call__(self, request):
        self.profiler = cProfile.Profile()
        response = self.profiler.runcall(self.get_response, request)
        self.profiler.create_stats()
        self.profiler.dump_stats(request.path.replace("/", "-") + ".prof")
        return response


class CachedPBKDF2PasswordHasher(PBKDF2PasswordHasher):
    """Trying https://forum.djangoproject.com/t/caching-in-auth-hashers/31372.

    I also reduced the iteration count because the default took 1s on
    my current server hardware:

    website@django1:~$ ./venv/bin/python ./src/manage.py shell -c "
    from django.contrib.auth.hashers import PBKDF2PasswordHasher;
    import time; start=time.perf_counter();
    PBKDF2PasswordHasher().encode('abc', 'def', 720000);
    end=time.perf_counter();
    print(end-start)"

    gives:

    1.071990156546235

    while for iteration=100000 it takes 0.14s, much more reasonable.
    """

    iterations = 100000

    def __init__(self, *args, **kwargs):
        self.success_cache = {}
        super().__init__(*args, **kwargs)

    def verify(self, password, encoded):
        if (password, encoded) in self.success_cache:
            return True
        result = super().verify(password, encoded)
        if result:
            self.success_cache[(password, encoded)] = True
        return result
