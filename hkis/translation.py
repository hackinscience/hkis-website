from modeltranslation.translator import TranslationOptions, translator

from .models import Exercise, Page, Tag


class ExerciseTranslationOptions(TranslationOptions):
    fields = ("title", "wording")


class TagTranslationOptions(TranslationOptions):
    fields = ("title", "description")


class PageTranslationOptions(TranslationOptions):
    fields = ("title", "body")


translator.register(Exercise, ExerciseTranslationOptions)
translator.register(Tag, TagTranslationOptions)
translator.register(Page, PageTranslationOptions)
