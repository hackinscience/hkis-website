from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.test import APITestCase

from hkis.admin import ExerciseAdmin
from hkis.models import Exercise, Tag

User = get_user_model()


class MockRequest:
    def __init__(self, user):
        self.user = user


class TestTeachersCanCreateExerciseViaAdmin(TestCase):
    fixtures = ["initial"]

    def test_create_exercise_via_admin(self):
        exercise_admin = ExerciseAdmin(model=Exercise, admin_site=AdminSite())
        a_teacher = User.objects.get(username="a-teacher")
        an_exercise = Exercise(title="Test", page_id=1)
        exercise_admin.save_model(
            obj=an_exercise, request=MockRequest(user=a_teacher), form=None, change=None
        )
        assert Exercise.objects.get(title="Test").author.username == "a-teacher"


class TestTeachersCanCreateExerciseViaAPI(APITestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.username = "a-teacher"
        user = User.objects.get(username=self.username)
        self.client.force_authenticate(user=user)

    def test_create_exercise_via_api(self):
        response = self.client.post(
            "/api/exercises/",
            {"title": "Test", "slug": "test", "page": "/api/pages/1/"},
        )

        assert response.status_code == 201
        assert self.client.get(response.data["url"]).data["title"] == "Test"

    def test_create_exercise_with_tag(self):
        response = self.client.post(
            "/api/exercises/",
            {
                "title": "Test",
                "slug": "test",
                "page": "/api/pages/1/",
                "tags": ["easy"],
            },
        )
        assert response.status_code == 201
        assert len(response.data["tags"]) == 1
        assert response.data["tags"][0]["slug"] == "easy"

    def test_create_exercise_with_tag_slug(self):
        response = self.client.post(
            "/api/exercises/",
            {
                "title": "Test",
                "slug": "test",
                "page": "/api/pages/1/",
                "tags": [{"slug": "the-slug"}],
            },
        )
        assert response.status_code == 201
        assert len(response.data["tags"]) == 1
        assert response.data["tags"][0]["slug"] == "the-slug"
        # We can't do magic about infering the title, but see 'test_reuse_tag' test.
        assert response.data["tags"][0]["title"] == "The-slug"

    def test_reuse_tag(self):
        Tag.objects.create(title="The Title", slug="the-slug")
        response = self.client.post(
            "/api/exercises/",
            {
                "title": "Test",
                "slug": "test",
                "page": "/api/pages/1/",
                "tags": [{"slug": "the-slug"}],
            },
        )
        assert response.status_code == 201
        assert len(response.data["tags"]) == 1
        assert response.data["tags"][0]["slug"] == "the-slug"
        assert response.data["tags"][0]["title"] == "The Title"

    def test_create_exercise_with_tag_title_via_api(self):
        assert Tag.objects.count()
        response = self.client.post(
            "/api/exercises/",
            {
                "title": "Test",
                "slug": "test",
                "page": "/api/pages/1/",
                "tags": [{"title": "The Title"}],
            },
        )
        assert response.status_code == 201
        assert len(response.data["tags"]) == 1
        assert response.data["tags"][0]["slug"] == "the-title"
        assert response.data["tags"][0]["title"] == "The Title"

    def test_create_exercise_with_tag_slug_and_title_via_api(self):
        response = self.client.post(
            "/api/exercises/",
            {
                "title": "Test",
                "slug": "title",
                "page": "/api/pages/1/",
                "tags": [{"slug": "the-slug", "title": "TheTitle"}],
            },
        )
        assert response.status_code == 201
        assert len(response.data["tags"]) == 1
        assert response.data["tags"][0]["slug"] == "the-slug"
        assert response.data["tags"][0]["title"] == "TheTitle"
        assert b"Test" in self.client.get(response.data["url"]).content

    def test_update_tag_via_api(self):
        an_exercise = Exercise.objects.filter(author__username=self.username).first()
        an_exercise.tags.clear()

        # Get an exercise
        response = self.client.get(an_exercise.get_api_url())
        document = response.data
        assert response.status_code == 200
        assert not response.data["tags"]  # has been .clear()ed a few lines back.
        document["tags"] = ["easy", "funny"]

        # Put it back
        response = self.client.put(an_exercise.get_api_url(), document)
        assert response.status_code == 200

        # Verify the put has worked as expected
        assert an_exercise.tags.count() == 2  # easy and funny
        response = self.client.get(an_exercise.get_api_url())
        document = response.data
        assert {tag["slug"] for tag in document["tags"]} == {"funny", "easy"}


class TestAdminStaffWithTeacherGroup(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.user = User.objects.get(username="a-teacher")
        self.client.force_login(self.user)

    def test_get_admin_exercises(self):
        response = self.client.get("/admin/hkis/exercise/")
        assert b"Hello World" not in response.content  # Exercise owned by a-superuser
        assert b"Print 42" in response.content  # Exercise owned by a-teacher

    def test_get_admin_exercises_1(self):
        """Exercise 3 is owned by mdk, a-teacher can't view it."""
        response = self.client.get("/admin/hkis/exercise/3/change/")
        assert response.status_code == 302

    def test_get_admin_exercises_2(self):
        """Exercise 5 is owned by a-teacher, a-teacher can view it."""
        response = self.client.get("/admin/hkis/exercise/5/change/")
        assert response.status_code == 200

    def test_create_exercise(self):
        response = self.client.post(
            "/admin/hkis/exercise/add/",
            {"title_en": "Lisa's exercise", "page": 1, "position": 100, "points": 1},
        )
        assert response.status_code == 302
        created = Exercise.objects.get(title="Lisa's exercise")
        assert created.is_published is False  # A staff can't self-publish exercises
        assert created.author == self.user


class TestTeachersCanViewStudentAnswersInAdmin(TestCase):
    """If a teacher is staff of a team, the teacher can see all
    answers from team members.
    """

    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.teacher = User.objects.get(username="a-teacher")
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")
        self.client.force_login(self.teacher)

    def test_admin_list_only_whats_needed(self):
        """In the admin interface, the list of answers should be
        filtered so the teachers only see what's interesting to them.
        """
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        own_answers = {answer.pk for answer in self.teacher.answer_set.all()}
        response = self.client.get("/admin/hkis/answer/")
        html = response.content.decode("UTF-8")
        for pk in student_answers | own_answers:
            assert f"/admin/hkis/answer/{pk}/change/" in html
        for pk in newbie_answers:
            assert f"/admin/hkis/answer/{pk}/change/" not in html

    def test_view_answer_in_admin(self):
        """The teacher should be able to see its student answers."""
        answer_ids = {answer.pk for answer in self.student.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/admin/hkis/answer/{answer_id}/change/")
            assert response.status_code == 200

    def test_view_own_answer_in_admin(self):
        """The teacher should be able to see its own answers."""
        answer_ids = {answer.pk for answer in self.teacher.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/admin/hkis/answer/{answer_id}/change/")
            assert response.status_code == 200

    def test_cannot_view_answer_in_admin(self):
        """The teacher should **not** be able to see other users answers."""
        answer_ids = {answer.pk for answer in self.newbie.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/admin/hkis/answer/{answer_id}/change/")
            assert response.status_code != 200


class TestTeachersCanViewStudentAnswersInAPI(APITestCase):
    """If a teacher is staff of a team, the teacher can access all
    answers from team members.
    """

    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.teacher = User.objects.get(username="a-teacher")
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")
        self.client.force_authenticate(user=self.teacher)

    def test_api_lists_only_whats_needed(self):
        """Over the API, the list of answers should be
        filtered so the teachers only see what's interesting to them.
        """

        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        own_answers = {answer.pk for answer in self.teacher.answer_set.all()}
        answers = self.client.get("/api/answers/?limit=100").content.decode("UTF-8")
        for pk in student_answers | own_answers:
            assert f"/api/answers/{pk}/" in answers
        for pk in newbie_answers:
            assert f"/api/answers/{pk}/" not in answers

    def test_get_answer_via_api(self):
        """The teacher should be able to see its student answers."""
        answer_ids = {answer.pk for answer in self.student.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/api/answers/{answer_id}/")
            assert response.status_code == 200

    def test_get_own_answer_via_api(self):
        """The teacher should be able to see its own answers."""
        answer_ids = {answer.pk for answer in self.teacher.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/api/answers/{answer_id}/")
            assert response.status_code == 200

    def test_cannot_get_answer_via_api(self):
        """The teacher should **not** be able to see other users answers."""
        answer_ids = {answer.pk for answer in self.newbie.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/api/answers/{answer_id}/")
            assert response.status_code != 200


class TestTeacherWithNoTeamCanSeeOwnAnswersInAdmin(TestCase):
    """If a teacher logs into the admin it should at least see its own answers."""

    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.teacher = User.objects.get(username="a-teacher-with-no-teams")
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")
        self.client.force_login(self.teacher)

    def test_admin_list_only_whats_needed(self):
        """In the admin interface, the teacher with no teams should
        see its own answers, and that's it.
        """
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        own_answers = {answer.pk for answer in self.teacher.answer_set.all()}
        response = self.client.get("/admin/hkis/answer/")
        html = response.content.decode("UTF-8")
        for pk in own_answers:
            assert f"/admin/hkis/answer/{pk}/change/" in html
        for pk in student_answers | newbie_answers:
            assert f"/admin/hkis/answer/{pk}/change/" not in html

    def test_view_own_answer_in_admin(self):
        """The teacher with no team should be able to see its own answers."""
        answer_ids = {answer.pk for answer in self.teacher.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/admin/hkis/answer/{answer_id}/change/")
            assert response.status_code == 200

    def test_cannot_view_answer_in_admin(self):
        """The teacher with no team should **not** be able to see
        other users answers."""
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        assert student_answers
        assert newbie_answers
        for answer_id in student_answers | newbie_answers:
            response = self.client.get(f"/admin/hkis/answer/{answer_id}/change/")
            assert response.status_code != 200


class TestTeacherWithNoTeamCanViewOwnAnswersInAPI(APITestCase):
    """If a teacher has no team it should still be able to view its
    own answers in the API."""

    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.teacher = User.objects.get(username="a-teacher-with-no-teams")
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")
        self.client.force_authenticate(user=self.teacher)

    def test_api_lists_only_whats_needed(self):
        """Over the API, the list of answers should be filtered so the
        teacher with no team only see its own answers.
        """
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        own_answers = {answer.pk for answer in self.teacher.answer_set.all()}
        answers = self.client.get("/api/answers/?limit=100").content.decode("UTF-8")
        for pk in own_answers:
            assert f"/api/answers/{pk}/" in answers
        for pk in student_answers | newbie_answers:
            assert f"/api/answers/{pk}/" not in answers

    def test_get_own_answer_via_api(self):
        """The teacher with no team should be able to see its own answers."""
        answer_ids = {answer.pk for answer in self.teacher.answer_set.all()}
        assert answer_ids
        for answer_id in answer_ids:
            response = self.client.get(f"/api/answers/{answer_id}/")
            assert response.status_code == 200

    def test_cannot_get_answer_via_api(self):
        """The teacher with no teams should **not** be able to see other users answers."""
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        assert student_answers
        assert newbie_answers
        for answer_id in student_answers | newbie_answers:
            response = self.client.get(f"/api/answers/{answer_id}/")
            assert response.status_code != 200
