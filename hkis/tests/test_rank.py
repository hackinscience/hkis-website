from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class TestRankUserWithInfo(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")

    def test_recompute_rank(self):
        self.student.recompute_points()
        assert User.with_rank.get(username="a-student").rank == 1

        self.newbie.recompute_points()
        assert User.with_rank.get(username="a-newbie").rank > 1

    def test_recompute_ranks(self):
        User.objects.recompute_points()
        assert User.with_rank.get(username="a-student").rank == 1
        assert User.with_rank.get(username="a-newbie").rank > 1


class TestRankUserWithNoInfo(TestCase):
    fixtures = ["initial"]

    def setUp(self):
        self.user = User.objects.create(username="Temporary")

    def test_recompute_ranks(self):
        User.objects.recompute_points()
