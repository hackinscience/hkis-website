from django.contrib.auth import get_user_model
from django.test import TestCase

from hkis.models import Exercise, Team

User = get_user_model()


class TestViewsAsSuperUser(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.username = "a-superuser"
        self.user = User.objects.get(username=self.username)
        self.client.force_login(self.user)

    def test_get_profile(self):
        response = self.client.get("/user/a-superuser")
        assert response.context["is_me"]

        response = self.client.get("/user/1", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-superuser"

        response = self.client.get("/user/a-teacher")
        assert not response.context["is_me"]

        response = self.client.get("/user/2", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-teacher"

    def test_update_my_profile(self):
        """Superuser can update profile from the admin but not from the website."""
        test_email = "test-update-profile@example.com"
        assert self.user.email != test_email
        response = self.client.post(
            "/user/a-superuser", {"username": "a-superuser", "email": test_email}
        )
        assert response.status_code < 400
        assert User.objects.get(username=self.username).email == test_email

    def test_get_teams(self):
        self.client.get("/teams/")
        self.client.get("/teams/team-of-user")
        self.client.get("/teams/team-of-user/stats")

    def test_get_page(self):
        response = self.client.get("/")
        assert response.context["tags"][0].solved == 0
        self.client.get("/exercises/")
        self.client.get("/exercises/?tag=django")
        self.client.get("/exercises/?resolved=1")
        self.client.get("/exercises/?resolved=0")
        self.client.get("/exercises/?resolved=1&tag=django")
        self.client.get("/exercises/?resolved=0&tag=django")
        self.client.get("/help/")

    def test_get_exercise(self):
        self.client.get("/exercises/hello-world")

    def test_get_solution(self):
        self.client.get("/exercises/hello-world/solutions")


class TestViewsAsTeacher(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.username = "a-teacher"
        self.user = User.objects.get(username=self.username)
        self.client.force_login(self.user)

    def test_get_profile(self):
        response = self.client.get("/user/a-superuser")
        assert not response.context["is_me"]

        response = self.client.get("/user/1", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-superuser"

        response = self.client.get("/user/a-teacher")
        assert response.context["is_me"]

        response = self.client.get("/user/2", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-teacher"

    def test_update_someone_else_profile(self):
        test_email = "test-update-profile@example.com"
        assert self.user.email != test_email
        response = self.client.post(
            "/user/a-superuser", {"username": "a-teacher", "email": test_email}
        )
        assert response.status_code == 403
        assert User.objects.get(username=self.username).email != test_email

    def test_get_teams(self):
        response = self.client.get("/teams/")
        assert response.status_code == 200
        response = self.client.get("/teams/team-of-teacher")
        assert response.status_code == 200
        response = self.client.get("/teams/team-of-teacher/stats")
        assert response.status_code == 200

    def test_stats(self):
        response = self.client.get("/stats/")
        assert response.status_code == 200

    def test_get_page(self):
        response = self.client.get("/")
        assert response.context["tags"][0].solved == 1
        self.client.get("/exercises/")
        self.client.get("/exercises/?tag=django")
        self.client.get("/exercises/?resolved=1")
        self.client.get("/exercises/?resolved=0")
        self.client.get("/exercises/?resolved=1&tag=django")
        self.client.get("/exercises/?resolved=0&tag=django")
        self.client.get("/help/")

    def test_get_solution(self):
        response = self.client.get("/exercises/hello-world/solutions")
        assert response.status_code == 200

    def test_unexisting_solution(self):
        response = self.client.get("/exercises/lsllkkkwkwjwhhwh/solutions")
        assert response.status_code == 404

    def test_get_exercises(self):
        response = self.client.get("/exercises/")
        assert response.status_code == 200
        assert len(response.context["exercises_done"]) > 0
        assert len(response.context["exercises_todo"]) > 0

    def test_get_exercise(self):
        url = Exercise.objects.first().get_absolute_url()
        response = self.client.get(url)
        assert response.status_code == 200
        assert response.context["is_valid"]
        assert "next" in response.context
        assert "previous" in response.context

    def test_get_team_stats(self):
        url = Team.objects.my_teams(self.user).first().get_absolute_url()
        response = self.client.get(url + "/stats")
        assert response.status_code == 200
        assert "exercises" in response.context
        assert "team" in response.context
        assert "table" in response.context

    def test_get_non_existing_team_stats(self):
        response = self.client.get("/teams/team-of-el-diablo/stats")
        assert response.status_code == 404

    def test_get_nonexisting_exercise(self):
        response = self.client.get("/exercises/aflskdslkslfslsfs")
        assert response.status_code == 404


class TestViewsAsStudent(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.username = "a-student"
        self.user = User.objects.get(username=self.username)
        self.client.force_login(self.user)

    def test_get_team_stats(self):
        """A team member (non staff) cannot see team stats."""
        response = self.client.get("/teams/team-of-teacher/stats")
        assert response.status_code == 403


class TestViewsAsAnon(TestCase):
    fixtures = ["initial", "test-exercises"]

    def test_get_profile(self):
        response = self.client.get("/user/a-superuser")
        assert not response.context["is_me"]
        response = self.client.get("/user/a-teacher")
        assert not response.context["is_me"]

    def test_non_existing_profile(self):
        response = self.client.get("/user/guido")
        assert response.status_code == 404

    def test_get_teams(self):
        response = self.client.get("/teams/")
        assert response.status_code == 200
        response = self.client.get("/teams/team-of-user")
        assert response.status_code == 200

    def test_get_non_existing_team(self):
        response = self.client.get("/teams/team-of-el-diablo")
        assert response.status_code == 404

    def test_get_team_stats(self):
        response = self.client.get("/teams/team-of-user/stats")
        assert response.status_code == 403

    def test_get_page(self):
        response = self.client.get("/")
        assert response.context["tags"][0].solved == 0
        self.client.get("/exercises/")
        self.client.get("/exercises/?tag=django")
        self.client.get("/exercises/?resolved=1")
        self.client.get("/exercises/?resolved=0")
        self.client.get("/exercises/?resolved=1&tag=django")
        self.client.get("/exercises/?resolved=0&tag=django")
        self.client.get("/help/")

    def test_get_solution(self):
        response = self.client.get("/exercises/hello-world/solutions")
        assert response.status_code == 302  # Login required

    def test_unexisting_solution(self):
        response = self.client.get("/exercises/lsllkkkwkwjwhhwh/solutions")
        assert response.status_code == 302  # Login required

    def test_old_profile_view_redirect(self):
        response = self.client.get("/profile/1", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-superuser"

        response = self.client.get("/profile/2", follow=True)
        assert response.redirect_chain[-1][0] == "/user/a-teacher"

    def test_get_exercises(self):
        response = self.client.get("/exercises/")
        assert response.status_code == 200
        assert len(response.context["exercises_done"]) == 0
        assert len(response.context["exercises_failed"]) == 0
        assert len(response.context["exercises_todo"]) > 0

    def test_get_exercise(self):
        for exercise in Exercise.objects.filter(is_published=True):
            response = self.client.get(exercise.get_absolute_url())
            assert response.status_code == 200
            assert not response.context["is_valid"]
            assert "next" in response.context
            assert "previous" in response.context

    def test_get_nonexisting_exercise(self):
        response = self.client.get("/exercises/alfsksdlsklslsffs")
        assert response.status_code == 404

    def test_team_leaderboard_redirect(self):
        response = self.client.get("/team-leaderboard/", follow=True)
        assert response.redirect_chain[-1][0] == "/teams/"
