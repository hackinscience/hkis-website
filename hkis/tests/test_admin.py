from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class TestAdminSuperUser(TestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.client.force_login(User.objects.get(username="a-superuser"))

    def test_get_admin_exercises(self):
        response = self.client.get("/admin/hkis/exercise/")
        assert b"Hello World" in response.content

    def test_get_admin_teams(self):
        response = self.client.get("/admin/hkis/team/")
        assert response.status_code == 200
        assert b"TeamOfTeacher" in response.content
        assert b"TeamOfUser" in response.content

    def test_get_admin_team(self):
        response = self.client.get("/admin/hkis/team/1/change/")
        assert response.status_code == 200
        assert b"TeamOfUser" in response.content

    def test_get_admin_exercises_1(self):
        response = self.client.get("/admin/hkis/exercise/3/change/")
        assert b"Hello World" in response.content
