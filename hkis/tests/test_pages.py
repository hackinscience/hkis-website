from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.test import APITestCase

from hkis.admin import PageAdmin
from hkis.models import Exercise, Page


class TestPages(TestCase):
    fixtures = ["initial"]

    def test_page(self):
        p1 = Page.objects.first()
        assert p1.slug in p1.get_absolute_url()


class MockRequest:
    def __init__(self, user):
        self.user = user


User = get_user_model()


class TestTeacherPermissionsInAdmin(TestCase):
    """A teacher can create a new page from the admin."""

    fixtures = ["initial"]

    def test_create_page_via_admin(self):
        title = "Test create page via admin"
        page_admin = PageAdmin(model=Page, admin_site=AdminSite())
        a_teacher = User.objects.get(username="a-teacher")
        a_page = Page(title=title)
        page_admin.save_model(
            obj=a_page, request=MockRequest(user=a_teacher), form=None, change=None
        )
        assert Page.objects.get(title=title).author.username == "a-teacher"


class TestPageAPI(APITestCase):
    fixtures = ["initial", "test-exercises"]

    def test_cannot_assign_exercise_to_not_owned_page(self):
        exercise = Exercise.objects.filter(author__username="a-teacher").first()
        page = Page.objects.exclude(author__username="a-teacher").first()
        self.client.force_authenticate(user=User.objects.get(username="a-teacher"))
        exercise_document = self.client.get(exercise.get_api_url()).data
        exercise_document["page"] = page.get_api_url()

        # Try to put the page change
        assert (
            self.client.put(exercise_document["url"], exercise_document).status_code
            == 400  # Bad request like:
            # {'page': ['The exercise author and the page owner have to be the same.']}
        )

    def test_teacher_can_create(self):
        self.client.force_authenticate(user=User.objects.get(username="a-teacher"))
        title = "Test create page via API"
        response = self.client.post(
            "/api/pages/",
            {"title": title, "slug": title.lower().replace(" ", "")},
        )
        assert response.status_code == 201
        assert self.client.get(response.data["url"]).data["title"] == title

    def test_user_cannot_create(self):
        self.client.force_authenticate(user=User.objects.get(username="a-student"))
        pages_before = Page.objects.count()
        title = "Test create page via API"
        response = self.client.post(
            "/api/pages/",
            {"title": title, "slug": title.lower().replace(" ", "")},
        )
        assert response.status_code == 403
        pages_after = Page.objects.count()
        assert pages_before == pages_after

    def test_teacher_can_edit_own_page(self):
        self.client.force_authenticate(user=User.objects.get(username="a-teacher"))
        # Searching my 'exercises' page:
        pages = self.client.get("/api/pages/").data
        exercise_page_url = [
            page for page in pages["results"] if page["slug"] == "exercises"
        ].pop()["url"]
        # Downloading my page
        exercise_page = self.client.get(exercise_page_url).data
        # Modifying it
        exercise_page["title"] = "Python page modified by teacher"
        # Putting it back
        assert self.client.put(exercise_page["url"], exercise_page).status_code == 200
        # Getting it back to check title
        exercise_page = self.client.get(exercise_page["url"]).data
        assert exercise_page["title"] == "Python page modified by teacher"

    def test_teacher_cannot_edit_not_owned_page(self):
        self.client.force_authenticate(user=User.objects.get(username="a-teacher"))
        # Searching my 'exercises' page:
        pages = self.client.get("/api/pages/").data
        c_page_url = [page for page in pages["results"] if page["slug"] == "C"].pop()[
            "url"
        ]
        # about is owned by superuser
        # Downloading the about page
        c_page = self.client.get(c_page_url).data
        # Modifying it
        c_page["title"] = "H4ck3d by 3v1l t34ch3r"
        # Putting it back
        assert self.client.put(c_page["url"], c_page).status_code == 403
        # Getting it back to check title
        c_page = self.client.get(c_page["url"]).data
        assert c_page["title"] == "C"

    def test_page_i18n(self):
        self.client.force_authenticate(user=User.objects.get(username="a-teacher"))
        # Searching my 'exercises' page:
        pages = self.client.get("/api/pages/").data
        exercise_page_url = [
            page for page in pages["results"] if page["slug"] == "exercises"
        ].pop()["url"]
        # Downloading my page in french
        exercise_page = self.client.get(
            exercise_page_url, headers={"Accept-Language": "fr"}
        ).data
        # Modifying it
        exercise_page["title"] = "Python page modified by teacher"
        # Putting the french one back
        assert (
            self.client.put(
                exercise_page["url"], exercise_page, headers={"Accept-Language": "fr"}
            ).status_code
            == 200
        )
        # Getting the french one back to check title
        exercise_page = self.client.get(
            exercise_page["url"], headers={"Accept-Language": "fr"}
        ).data
        assert exercise_page["title"] == "Python page modified by teacher"

        # Getting the english one back to check title
        exercise_page = self.client.get(
            exercise_page["url"], headers={"Accept-Language": "en"}
        ).data
        assert exercise_page["title"] == "Exercises"
