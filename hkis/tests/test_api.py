from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase

User = get_user_model()


class TestAPIAnswerAnonymous(APITestCase):
    fixtures = ["initial", "test-exercises"]

    def test_get_answer(self):
        response = self.client.get("/api/answers/")
        assert response.status_code == 403


class TestAPIAnswerAuthed(APITestCase):
    fixtures = ["initial", "test-exercises"]

    def setUp(self):
        self.student = User.objects.get(username="a-student")
        self.newbie = User.objects.get(username="a-newbie")
        self.client.force_authenticate(user=self.student)

    def test_get_answers(self):
        response = self.client.get("/api/answers/")
        assert response.status_code == 200
        assert response.data["results"]

    def test_list_own_answers(self):
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        response = self.client.get("/api/answers/").content.decode("UTF-8")
        for pk in student_answers:
            assert f"/api/answers/{pk}/" in response

    def test_get_own_answers(self):
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        assert student_answers
        for pk in student_answers:
            response = self.client.get(f"/api/answers/{pk}/")
            assert response.status_code == 200

    def test_delete_own_answers(self):
        student_answers = {answer.pk for answer in self.student.answer_set.all()}
        for pk in student_answers:
            response = self.client.delete(f"/api/answers/{pk}/")
            assert response.status_code == 204

    def test_cannot_see_other_answers(self):
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        assert newbie_answers
        response = self.client.get("/api/answers/").content.decode("UTF-8")
        for pk in newbie_answers:
            assert f"/api/answers/{pk}/" not in response

    def test_cannot_delete_other_answers(self):
        newbie_answers = {answer.pk for answer in self.newbie.answer_set.all()}
        assert newbie_answers
        for pk in newbie_answers:
            response = self.client.delete(f"/api/answers/{pk}/")
            assert response.status_code >= 400
