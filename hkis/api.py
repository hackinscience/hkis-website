import django_filters
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth.models import Group
from rest_framework import filters, permissions, response, routers, viewsets
from rest_framework.permissions import DjangoModelPermissions

from hkis.models import Answer, Exercise, Membership, Page, Tag, Team, User
from hkis.serializers import (
    AnswerSerializer,
    ExerciseSerializer,
    ExerciseSerializerFull,
    GroupSerializer,
    MembershipSerializer,
    PageSerializer,
    TagSerializer,
    TeamSerializer,
    UserSerializer,
)


class DjangoModelPermissionsLax(DjangoModelPermissions):
    authenticated_users_only = False


class DjangoModelPermissionsStrict(DjangoModelPermissions):
    perms_map = {
        "GET": ["%(app_label)s.view_%(model_name)s"],
        "OPTIONS": [],
        "HEAD": [],
        "POST": ["%(app_label)s.add_%(model_name)s"],
        "PUT": ["%(app_label)s.change_%(model_name)s"],
        "PATCH": ["%(app_label)s.change_%(model_name)s"],
        "DELETE": ["%(app_label)s.delete_%(model_name)s"],
    }


class PagePermission(permissions.BasePermission):
    """Only page owners can modify / delete them.

    Staff can create pages (so teachers are allowed to create
    pages, not just superusers).
    """

    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or request.user.is_staff

    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS or obj.author == request.user


class TeamPermission(permissions.BasePermission):
    """Anyone can create teams."""

    def has_permission(self, request, view):
        if request.method == "POST":
            return request.user.is_authenticated
        return True  # let has_object_permission handle it.

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if obj.is_staff(request.user):
            return True
        return False


class ExercisePermission(DjangoModelPermissions):
    """Allow exercise author to modify them.

    Also anyone should have the right to create new exercises.
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        return request.method in permissions.SAFE_METHODS or (
            request.user.is_authenticated and obj.author == request.user
        )


class MembershipPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.method == "PUT" and obj.team.is_staff(request.user):
            return True
        if request.method == "DELETE" and (
            obj.team.is_staff(request.user) or obj.user == request.user
        ):
            return True
        return False


class AnswerPermission(permissions.BasePermission):
    """Permission to only allow answers to be:

    - Created by anyone, but not modified.
    - Seen (read-only) by their owners only.

    There's no need to specify permission at object level because the
    QuerySet of answers visible via the API is already constrained.
    """

    update_allows_field = ["is_shared"]

    def has_permission(self, request, view):
        """Authenticated users can create (POST) but not edit."""
        if not request.user.is_authenticated:
            return False
        if request.method in ("POST", "DELETE"):
            # Logged in user can answer and delete own answers
            return True
        if request.method == "PATCH":
            # We authorized patch only if the field is authorized
            return all(key in self.update_allows_field for key in request.data.keys())
        if request.method in permissions.SAFE_METHODS:
            return True
        return False


class UsersViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissionsStrict]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class MembershipFilter(django_filters.rest_framework.FilterSet):
    user = django_filters.NumberFilter(field_name="user")
    team = django_filters.NumberFilter(field_name="team")

    class Meta:
        model = Membership
        fields = ["user", "team"]


class MembershipViewSet(viewsets.ModelViewSet):
    permission_classes = [MembershipPermission]
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
    filterset_class = MembershipFilter

    def perform_destroy(self, instance):
        super().perform_destroy(instance)
        instance.team.ensure_has_staff_or_drop_team()


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissionsStrict]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class QSearchFilter(filters.SearchFilter):
    search_param = "q"


class TeamViewSet(viewsets.ModelViewSet):
    permission_classes = [TeamPermission]
    filter_backends = [QSearchFilter]
    search_fields = ["name"]
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

    def get_queryset(self):
        return Team.objects.teams_i_can_see(self.request.user)

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.add_member(self.request.user)


class TagViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissionsLax]
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class PageViewSet(viewsets.ModelViewSet):
    permission_classes = [PagePermission]
    queryset = Page.objects.all()
    serializer_class = PageSerializer


class AnswerFilter(django_filters.rest_framework.FilterSet):
    username = django_filters.CharFilter(field_name="user__username")

    class Meta:
        model = Answer
        fields = ("is_corrected", "is_valid", "exercise")


class AnswerViewSet(viewsets.ModelViewSet):
    permission_classes = [AnswerPermission]
    queryset = Answer.objects.all()
    filterset_class = AnswerFilter
    serializer_class = AnswerSerializer

    def cb_new_answer(self, instance):
        group = f"answers.{instance.user.id}.{instance.exercise.id}"
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            group,
            {
                "type": "correction",
                "exercise": instance.exercise.id,
                "correction_message": instance.correction_message,
                "answer": instance.id,
                "is_corrected": instance.is_corrected,
                "is_valid": instance.is_valid,
            },
        )

    def get_queryset(self):
        return Answer.objects.visible_by(self.request.user).order_by("-pk")

    def perform_update(self, serializer):
        instance = serializer.save()
        if "is_corrected" in serializer.validated_data:
            self.cb_new_answer(instance)

    def perform_create(self, serializer):
        instance = serializer.save(user=self.request.user)
        self.cb_new_answer(instance)


class ExerciseFilter(django_filters.rest_framework.FilterSet):
    page = django_filters.CharFilter(field_name="page__slug")


class ExerciseViewSet(viewsets.ModelViewSet):
    permission_classes = [ExercisePermission]
    queryset = Exercise.objects.prefetch_related("tags").select_related("page")
    filterset_class = ExerciseFilter

    def get_serializer_class(self):
        if self.action == "list":
            return ExerciseSerializer
        else:
            return ExerciseSerializerFull

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class UserView(viewsets.ViewSet):
    def list(self, request):
        if request.user.is_anonymous:
            return response.Response(status=404)
        return response.Response(
            UserSerializer(request.user, context={"request": request}).data
        )

    def get_view_name(self):
        return "Me"


router = routers.DefaultRouter()
router.register("answers", AnswerViewSet)
router.register("exercises", ExerciseViewSet)
router.register("groups", GroupViewSet)
router.register("teams", TeamViewSet)
router.register("users", UsersViewSet)
router.register("tags", TagViewSet)
router.register("pages", PageViewSet)
router.register("memberships", MembershipViewSet)
router.register("me", UserView, basename="me")
urls = router.urls
