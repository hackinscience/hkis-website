Running an Genepy Server
########################

In this section we'll see how to run an GENEPY instance in production.

To just run one locally, see the :ref:`contributing` section.

The way I use to push genepy to production is Ansible, you can take a
look at the ansible playbooks in the ``deploy/`` directory, but all
the details can be found below.


Running the website
*******************

Running the website needs Python 3.8+.

Running the website needs a few non-Python dependencies:

- postgresql (or SQLite or even MySQL)
- redis (to dispatch answer to correction bots)
- nginx (for HTTPS decapsulation and to dispatch HTTP requests to Daphne or gunicorn)

Those can be installed on a Debian or Debian-based distrib using::

  apt install redis postgresql nginx


Simple mode (asgi only)
=======================

Genepy uses websockets, so it runs usng daphne_.

In production you can start it like this::

  daphne genepy_org.asgi:application

You can have ``daphne`` listen on a socket using ``-u
/path/to/socket``, naming sockets is more verbose than assigning port
numbers.

From an nginx point of view, it looks like::

  location / {
       proxy_pass http://unix:/path/to/daphne.sock;
       proxy_http_version 1.1;
       proxy_set_header Host $host;
       proxy_set_header X-Forwarded-Protocol $scheme;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "Upgrade";
   }


Dual stack (wsgi+asgi)
======================

You can optionally run genepy in a dual stack mode, to
load-balance between a ``daphne`` server for asynchronous requests and
a ``gunicorn`` server for synchronous ones.

In this case:

- All urls starting with ``/ws/`` are to be routed to a daphne_ instance.
- All other urls are to be routed to a gunicorn_ or uwsgi_ instance.

In the case of ``gunicorn`` it can be started like this::

  gunicorn genepy_org.wsgi

From an nginx point of view it looks like::

    location /ws {
        proxy_pass http://unix:/path/to/daphne.sock;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location / {
        proxy_pass http://unix:/path/to/gunicorn.sock;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Protocol $scheme;
    }


Running the correction server
*****************************

Correction bots are using Redis to communicate to the
website, idea is you can have as many machine as needed to run
correction. Starting with 2 is OK.

Correction bots are using firejail_ to run students code in a safer
way, you have to install it::

  apt install firejail

Correction machines don't need much CPU, but don't give them less than
2GB of RAM so we can spot problems (and reports them cleanly to the
user) before OOM killer spots them (and just kills the script).

To run a correction bot in production you just need to run::

  python correction_bot.py

A good way to do it is to use systemd as is::

  [Unit]
  Description=Correction bot
  After=network.target

  [Service]
  User=genepy-correction-bot
  Group=genepy-correction-bot
  WorkingDirectory=/path/to/genepy-website/clone/
  ExecStart=/path/to/a/venv/bin/python correction_bot.py
  Environment="CORRECTION_BOT_INTERPRETER=/path/to/dedicated/jail/venv/bin/python"
  Restart=always

  [Install]
  WantedBy=multi-user.target


You can choose which Python intepreter used to run corrections by
settings the `CORRECTION_BOT_INTERPRETER` environment variable. It
allows to use a venv, which allows to install dependencies like
correction-helper_.

Beware: do not put this venv inside the correction bot home: the
sandbox would hide it.

.. _correction-helper: https://pypi.org/p/correction-helper/
.. _firejail: https://github.com/netblue30/firejail
.. _daphne: https://github.com/django/daphne/
.. _gunicorn: https://gunicorn.org/
.. _uwsgi: https://uwsgi-docs.readthedocs.io/en/latest/
