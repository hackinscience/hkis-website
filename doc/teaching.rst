Teaching using Genepy
#####################

So you got teacher access? Welcome!

Don't hesitate to give me feedback about this page or the admin
interface, if you're left with unanswered questions, I'll gladly
enhance it (PR are welcome too!).


Accessing the admin area
************************

In the top right of the page you should have a new menu item named
`Admin`, you can click on it to access the admin area.

In the admin you can see:

- Pages
- Exercises
- Answers
- Teams


Pages
*****

What I call "a page" is an URL like `/exercises/` which hosts a set of
exercises.

A page also has a title and can contain some text.

In the page admin you see only your pages, you can create your first
page by clicking the "Add" button.

Start with just a title, maybe a word or two in the body and click
"SAVE".

Your page has now an address and exists on the website.

Clicking on a page allows two things: editing the page, but also while
editing a page you can hit a "See on website" button, this is
nice to see what your page looks like.


Exercises
*********

Now that you have a page you can put exercises on it. Click the "Add"
button on the "Exercises" admin to create one.

Writing exercises is a big task, I wrote a page dedicated to it: :ref:`writing-exercises`.

But let's start with a simple exercise:

- Choose a title, like "test"
- You can leave the slug empty, it'll be auto-generated from the title.
- Select your page
- Check ``is_published`` to make it visible on your page.
- Write a few words in the wording area, just so it's not empty, don't mind the translations.
- Leave the initial solution empty
- In the ``check.py`` write ``print("Congratulations!!")``, that's not a very strict check, but to test it'll be OK.
- Click "SAVE".


To see your exercise just refresh the page you put your exercise in, it should be here now. There's also a "see on website" button on the exercise modification page.

You can now test your exercise: put something in the left area, click "Submit"
and you should see the ``Congratulations!!`` messages appear on the
right.


Answers
*******

Now that you have an exercise and one answer, you can take a look at
the "Answers" admin.

Here you should see your answer, click on it to see more details about it.

This admin page allows you to see the answers of your students.


Teams
*****

Teams are the way to tell who are your students.

This is usefull because you can only see answers of your students.

To create a team clik the "Add" button on the "Teams" admin.

A team is just a name, but you can choose to make it public or not
(public teams appears publicly on team list on the website).

Once created you have to ask your students to join the team from their
profile, hopefully there's autocompletion here so they will find the
right team easily.
