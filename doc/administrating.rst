Administrating Genepy server
============================

So you just installed Genepy what's next?


Creating an admin account
-------------------------

The first thing you'll need is an admin account, this is the only step
to do server-side on the command-line. It should be executed in the
root directory of genepy, where's `./manage.py` is::

    ./manage.py createsuperuser


Accessing the admin area
------------------------

Open your browser, go to your Genepy host and login. You'll see an
``Admin`` link in the header.

It's often faster to just aim to `/admin/` manually though, once your
browser knows it.


Creating an exercise
--------------------

Genepy can host multiple exercises page, but by default there's none,
so it's time to create one, name it `exercises`.


Creating exercises
------------------

See :ref:`writing-exercises`.
