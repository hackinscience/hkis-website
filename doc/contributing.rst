Contributing
############

Running the website locally
***************************

To run locally you'll need a venv::

  python -m venv .venv
  source .venv/bin/activate

You'll need to install some dependencies::

  pip install requirements.txt

You'll also need a local database (no need to install postgresql,
we're using sqlite3 in development)::

  ./manage.py migrate

Now that we have a database, let's load some examle data in it::

  ./manage.py loaddata initial

While we're at it, let's create an admin account for you::

  ./manage.py createsuperuser

You're ready to run the website::

  ./manage.py runserver

That should work but you don't have a correction bot yet. To start a
correction bot you'll need to install a redis server::

  sudo apt install redis

And to secure the execution of the exercises we use firejail, install
it too::

  sudo apt install firejail

(or whatever installation thing you use on your distrib).

The correction bot will need to setup a venv outside /home and /tmp,
you can help it:

  sudo mkdir /srv/bot/
  sudo chown $USER:$USER /srv/bot/

You now can run a correction bot::

  python manage.py bot

Now you can login to ``/admin``, from here you can change everything
about your installation: manage users, create pages, create exercises,
teams, review answers, and so on.

A ``/exercises`` page has been created automatically for you, you may
want to start by adding some exercises to it.


Updating the initial fixture
****************************

To save the fixtures files, use::

  ./manage.py dumpdata --indent 4 hkis.userinfo hkis.page hkis.tag hkis.team hkis.membership auth.user -o hkis/fixtures/initial.json

  ./manage.py dumpdata --indent 4 hkis.exercise hkis.answer -o hkis/fixtures/exercises.json


Translating
***********

Templates are translated using django `makemessages` and
`compilemessages` commands::

  ./manage.py makemessages --locale fr
  ./manage.py makemessages --locale fr --domain djangojs

Edit the po files and run::

  ./manage.py compilemessages --ignore .tox --ignore .venv

Exercises (title and wording) are translated via the admin (or the
API), we use ``django-modeltranslation``.
