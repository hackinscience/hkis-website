# Deploying Genepy

## Trying the playbooks on a container

You may want to test the playbooks on your local computer before
pushing it to a real production environment, here how I do it:

    incus launch images:debian/12 genepy
    incus exec genepy apt install openssh-server python3 rsync

Setup your SSH key on it:

    ssh-add -L | incus exec genepy tee /root/.ssh/authorized_keys

Get the machine IP address :

    incus list  # to get the IP, like 10.221.134.65

In the inventory, configure it like:

    [website]
    genepy ansible_host=10.221.134.65

And run ansible:

    ansible-playbook website.yml

## pgbadger

Use as:

    ssh root@genepy.org pgbadger /var/log/postgresql/postgresql-15-main.log
    rsync root@genepy.org:out.html ./


## TODO

- On the correction bot we need to install pandas, numpy, and freezgun


## Pre-requires

Only prerequisite is Ansible, so maybe use a venv, like:

```
$ python3 -m venv .venv/
$ . .venv/bin/activate
$ pip install -r requirements.txt
```


## Find the vault key

Maybe just ask Julien for the vault key and place it in
`~/.ansible-hkis-vault`.


## Run ansble

``` ansible-playbook site.yml ```

Or to update only parts:

- `ansible-playbook website.yml`
- `ansible-playbook moulinette.yml`


## To restore a backup

    sudo --user website psql -d hkis < backup.sql
