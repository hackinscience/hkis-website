---

- name: APT update cache (mandatory on first run)
  apt:
    update_cache: yes
    cache_valid_time: 86400

- name: Install dependencies
  apt:
    name:
      - cron
      - build-essential
      - firejail
      - gettext
      - git
      - python3-full
      - valgrind
      - rustc
    state: present

# This is the venv used at runtime by the jailed bot.  It's usefull so
# the moulinette can have a few dependencies like correction-helper.
# This is **not** the venv used by the correction-bot service.
- name: Create a directory for the jail venv
  file:
    path: "{{ correction_bot_jail_venv }}"
    state: directory
    mode: 0755

- name: Setup or upgrade jail venv
  command: python3 -m venv --upgrade-deps "{{ correction_bot_jail_venv }}"
  changed_when: false

- name: Install jail dependencies
  pip:
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: "{{ correction_bot_jail_venv }}"
    name: "{{ correction_bot_jail_dependencies }}"

- name: Add unix user correction-bot
  user:
    name: correction-bot
    shell: /bin/false
    system: yes
    home: "{{ correction_bot_home }}"

- name: install correction-bot.service
  template:
    src: correction-bot.service.j2
    dest: /etc/systemd/system/correction-bot.service
    owner: root
    group: root
    mode: 0644
  notify: restart correction bot

- name: git clone
  git:
    repo: https://git.afpy.org/mdk/genepy
    dest: "{{ correction_bot_home}}/src"
    force: yes
    accept_hostkey: yes
  notify: restart correction bot

- name: git clone exercises repo (for i18n)
  git:
    repo: https://git.afpy.org/mdk/genepy-exercises
    dest: /srv/exercises/
    force: yes
    accept_hostkey: yes

- name: Compile po file
  shell: msgfmt /srv/exercises/locale/fr/LC_MESSAGES/check.po -o /usr/share/locale/fr/LC_MESSAGES/hkis.mo

- name: Create a directory for the correction bot venv
  file:
    path: "{{ correction_bot_home }}/venv"
    state: directory
    mode: 0755

- name: pip upgrade to get cryptography wheels
  pip:
    chdir: "{{ correction_bot_home }}/src"
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: "{{ correction_bot_home }}/venv"
    name:
      - "pip>=21.0.1"
      - "setuptools>=53.0.0"
      - "wheel>=0.36.2"
  notify: restart correction bot

- name: pip installs requirements
  pip:
    chdir: "{{ correction_bot_home }}/src"
    requirements: requirements.txt
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: "{{ correction_bot_home }}/venv"
  notify: restart correction bot

- name: Ensure correction-bot is running
  service: name=correction-bot state=started enabled=yes
