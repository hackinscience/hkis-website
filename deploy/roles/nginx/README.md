# Nginx with Letsencrypt

This role sets up nginx with letsencrypt (using DNS-01 with Gandi API) .


## Role Variables

The mandatory variables are:

- `admin_email`: For letsencrypt.
- `nginx_certificates`: A list of domain to put in this certificate.
- `nginx_domain`: Used for file names, certificate name, and default server_name if no nginx_conf is given.
- `nginx_conf`: The nginx config.

For RFC 2136 use the following variables:

- `dns_rfc2136_server`
- `dns_rfc2136_name`
- `dns_rfc2136_secret`
- `dns_rfc2136_algorithm`

For Gandi API use:

- `vault_gandi_pat`: A Gandi Personal Access Token, generate it from the organization view in your Gandi dashboard.


Optional variables are:

- `nginx_owner`: If a unix user has to be created for this project.
- `nginx_path`: To create a directory owned by `nginx_owner`.


### Author Information

Julien Palard — https://mdk.fr
